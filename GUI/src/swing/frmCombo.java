package swing;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


//La clase frmCombo extiende de JFrame e implementa la interfaz ActionListener,
//lo que significa que manejará eventos de acción generados por los componentes de la interfaz de usuario.
public class frmCombo extends JFrame implements ActionListener
{
	//Se declara un campo private JComboBox<clsPersona> comboBox; Esto define un JComboBox que contendrá objetos de tipo clsPersona.
	private JComboBox <clsPersona> comboBox;

	
	public frmCombo()
	{
		//En el constructor frmCombo(), se inicializa la ventana y se configura su título, tamaño y operación de cierre.
		this.setTitle("JComboBox");
		this.setBounds(100, 100, 450, 300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//this.getContentPane().setLayout(null); establece el diseño del contenido del panel a nulo,
		//lo que significa que los componentes se pueden ubicar de manera absoluta.
		this.getContentPane().setLayout(null);
		
		//comboBox = new JComboBox<clsPersona>(); crea un nuevo JComboBox que contendrá objetos de tipo clsPersona.
		comboBox = new JComboBox <clsPersona> ();

		//comboBox.setSize(225, 25); y comboBox.setMaximumSize(new Dimension(225, 25)); establecen el tamaño del JComboBox.
		comboBox.setSize(225, 25);
		comboBox.setMaximumSize(new Dimension(225, 25));

		//comboBox.setLocation(100, 100); establece la posición del JComboBox dentro del JFrame.
		comboBox.setLocation(100, 100);

		//comboBox.addActionListener(this); registra esta instancia de frmCombo como oyente de eventos de acción para el JComboBox.
		comboBox.addActionListener(this);

		//this.getContentPane().add(comboBox); agrega el JComboBox al contenido del panel.
		this.getContentPane().add(comboBox);
		
	}
	
	//public void setItem(clsPersona persona) es un método que permite agregar elementos de tipo clsPersona al JComboBox.
	public void setItem(clsPersona persona)
	{
		this.comboBox.addItem(persona);
	}

	//En el método actionPerformed(ActionEvent arg0), se maneja el evento de acción generado por el JComboBox cuando se selecciona un elemento.
	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		//Se obtiene el objeto seleccionado del JComboBox con comboBox.getSelectedItem() y se lo convierte al tipo clsPersona con (clsPersona).
		//Luego se llama al método getDni() del objeto clsPersona seleccionado para obtener el DNI.
		String dni = ((clsPersona) comboBox.getSelectedItem()).getDni();

		//Finalmente, se imprime el DNI en la consola.
		System.out.println("El DNI de esta persona es " + dni);
		
	}
}
