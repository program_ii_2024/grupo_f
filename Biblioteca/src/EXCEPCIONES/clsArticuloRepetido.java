package EXCEPCIONES;

/**
 * Clase que gestiona la excepcion explicita producida por un articulo repetido
 * @author Manel D.
 * @author Juan Mari D.
 * @author Jon T.
 * @author Ruben A.
 */
public class clsArticuloRepetido extends Exception
{
    /**
     * Atributo del codigo del articulo
     */
    private String str_codigo;

    /**
     * Metodo constructor
     * @param str_paramCodigo El codigo del articulo
     */
    public clsArticuloRepetido(String str_paramCodigo)
    {
        super();
        this.str_codigo = str_paramCodigo;
    }

    /**
     * Metodo que devuelve un string con el mensaje de error
     * @return El string con el error
     */
    public String mensajeError()
    {
        return "El codigo " + this.str_codigo + " se encuentra repetido.";
    }
}