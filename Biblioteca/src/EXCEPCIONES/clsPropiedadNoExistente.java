package EXCEPCIONES;

/**
 * Clase que gestiona la excepcion implicita producida por la falta de 
 * itfProperty
 * @author Manel D.
 * @author Juan Mari D.
 * @author Jon T.
 * @author Ruben A.
 */
public class clsPropiedadNoExistente extends RuntimeException 
{
    /**
     * Propiedad solicitada y que no existe en el objeto correspondiente.
     */
    private String str_propiedad;

    /**
     * Metodo constructor
     */
    public clsPropiedadNoExistente()
    {
        this.str_propiedad = null;
    }

    /**
     * Metodo que gestiona el atributo de esta clase
     * @param str_paramPropiedad Propiedad que queremos gestionar
     */
    public clsPropiedadNoExistente(String str_paramPropiedad) 
    {
        this.str_propiedad = str_paramPropiedad;
    }

    /**
     * Metodo que devuelve un string con el mensaje de error
     * @return El string con el error
     */
    public String mensajeError()
    {
        return "La propiedad: " + this.str_propiedad + " no existe.";
    }
}