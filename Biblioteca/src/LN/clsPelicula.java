package LN;

import COMUN.clsConstantes;

/**
 * Clase que contiene los metodos que corresponden a la gestion de peliculas
 * @author Manel D.
 * @author Juan Mari D.
 * @author Jon T.
 * @author Ruben A.
 */
public final class clsPelicula extends clsArticulo 
{
    /**
     * Atributo del director de la pelicula
     */
    private String str_director;

    /**
     * Constructor para inicializar el atributo
     */
    public clsPelicula() 
    {
        super();
        this.str_director = "";
    }

    /**
     * Metodo que gestiona los atributos de la clase {@link clsPelicula}
     * @param str_paramTitulo Parametro que guarda el titulo de la pelicula
     * @param en_paramGenero  Parametro que guarda el genero de la pelicula
     * @param int_paramPrecio Parametro que guarda el precio de la pelicula
     * @param str_paramCodigo Parametro que guarda el codigo de la pelicula
     * @param str_paramDirector Parametro que guarda el director de la pelicula
     */
    public clsPelicula( String str_paramTitulo, enTipoGenero en_paramGenero,
                        int int_paramPrecio, String str_paramCodigo, 
                        String str_paramDirector) 
    {
        super(  str_paramTitulo, en_paramGenero, int_paramPrecio, 
                str_paramCodigo);
        this.str_director = str_paramDirector;
    }

    /**
     * Metodo que recoge el director de cada pelicula
     * @return Retorna el director de la pelicula
     */
    public String getDirector() 
    {
        return this.str_director;
    }

    /**
     * Metodo que establece el director de cada pelicula
     * @param str_paramDirector Parametro que guarda el director de la pelicula
     */
    public void setDirector(String str_paramDirector) 
    {
        this.str_director = str_paramDirector;
    }

    @Override
    /**
     * Metodo toString, que convierte los atributos en una cadena para
     * poder mostrarlos en pantalla
     * @return retorna los atributos de la clase {@link clsPelicula}
     */
    public String toString()
    {
        String str_articulo;
        str_articulo = super.toString();
        return str_articulo + " Director: " + str_director;
    }

    @Override
    /**
     * Metodo que recoge las propiedades gestionadas por la clase itfProperty.
	 * @param str_paramPropiedad Parametro que guarda la propiedad.
     */
    public Object getObjectProperty(String str_paramPropiedad) 
    {
        switch( str_paramPropiedad ) 
        {
            case clsConstantes.DIRECTOR : return this.str_director;
            default : return super.getObjectProperty(str_paramPropiedad);
        }
    }
}

