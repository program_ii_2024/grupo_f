package LN;
import java.util.Random;

public class clsHilo extends Thread {
    @Override
    public void run() {
        // Bucle infinito para ejecutar el hilo continuamente
        while (true) {
            // Generar un número aleatorio entre 0 y 99 para representar el número de personas conectadas
            int personasConectadas = new Random().nextInt(100);
            
            // Mostrar el mensaje por pantalla
            System.out.println("Actualmente hay " + personasConectadas + " personas conectadas a la página de la biblioteca");
            
            try {
                // Dormir el hilo durante 10 segundos
                Thread.sleep(100000); // 10000 milisegundos = 10 segundos
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        // Crear e iniciar un nuevo hilo
        clsHilo hilo = new clsHilo();
        hilo.start();
    }
}








