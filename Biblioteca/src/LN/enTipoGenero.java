package LN;

/**
 * Enumerado que guarda los tipos de generos que pueden tener los articulos
 * @author Manel D.
 * @author Juan Mari D.
 * @author Jon T.
 * @author Ruben A.
 */
public enum enTipoGenero 
{

    /**
     * Historico
     */
    en_historico(1),

    /**
     * De terror
     */ 
    en_terror(2),

    /**
     * De accion
     */
    en_accion(3), 

    /**
     * Romantico
     */
    en_romantico(4);

    /**
     * Valor del enumerado
     */
    private int int_valorTipoGenero;
    
    /**
     * Metodo que gestiona el enumerado
     * @param en_paramTipoGenero Tipo de genero
     */
    private enTipoGenero(int en_paramTipoGenero)
    {
        this.int_valorTipoGenero = en_paramTipoGenero;
    }
}
