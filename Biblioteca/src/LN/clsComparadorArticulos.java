package LN;

import java.util.Comparator;

/**
 * Este comparador ordena por titulo ascendente
 */
public class clsComparadorArticulos implements Comparator<clsArticulo> 
{

    /**
     * atributo booleano para comparar los articulos
     */
    public boolean ascendente;

    /**
     * Metodo de comparador de articulos
     * @param ascendente Ascendente
     */
    public clsComparadorArticulos(boolean ascendente) 
    {
        this.ascendente = ascendente;
    }

    @Override
    /**
     * Metodo que compara dos articulos
     */
    public int compare(clsArticulo o1, clsArticulo o2) 
    {
        if (ascendente)
            return o1.getTitulo().compareTo(o2.getTitulo());
        else
            return o2.getTitulo().compareTo(o1.getTitulo());
    }
}
