package LN;

import COMUN.*;
import COMUN.clsConstantes.en_Rol;

/**
 * Clase que contiene los metodos que corresponden a la gestion del rol
 * visitante
 * @author Manel D.
 * @author Juan Mari D.
 * @author Jon T.
 * @author Ruben A.
 */
public class clsVisitante extends clsUsuario 
{

   /**
    * atributo codigo de los visitantes
    */
   private String str_fechaVisita;

   /**
    * Este metodo es el metodo constructor de los usuarios clsVisitante
    * @param str_nombre Nombre del usuario
    * @param str_email Email del usuario
    * @param rol Rol del usuario
    * @param int_Id ID del usuario
    * @param str_fechaVisita Fecha de la Visita
    */
   public clsVisitante(String str_nombre, String str_email, en_Rol rol, 
                       int int_Id, String str_fechaVisita) 
   {
      super(str_nombre, str_email, rol, int_Id);
      this.str_fechaVisita = str_fechaVisita;
   }

   /**
    * Metodo que permite la consulta de la fecha de Visita
    * @return Fecha de Visita
    */
   public String getfechaVisita() 
   {
      return this.str_fechaVisita;
   }
   
   /**
    * Permite introducir la fecha de Visita
    * @param str_fechaVisita Fecha de la Visita
    */
   public void setfechaVisita(String str_fechaVisita) 
   {
      this.str_fechaVisita = str_fechaVisita;
   } 

   @Override
   /**
	* Metodo que recoge las propiedades gestionadas por la clase itfProperty.
	* @param str_paramPropiedad Parametro que guarda la propiedad.
	*/
   public Object getObjectProperty(String str_paramPropiedad)
   {
      switch ( str_paramPropiedad ) 
        {
            case clsConstantes.FECHA_VISITA: return str_fechaVisita;  
            default: return super.getObjectProperty(str_paramPropiedad);
        }
   }

   @Override
   /**
    * Metodo para comparar usuarios que se usara mas adelante
    */
   public int compareTo(clsUsuario o) 
   {
      return 0;
   }
}