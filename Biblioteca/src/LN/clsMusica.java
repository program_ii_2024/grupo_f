package LN;

import COMUN.clsConstantes;

/**
 * Clase que contiene los metodos que corresponden a la gestion de musica
 * @author Manel D.
 * @author Juan Mari D.
 * @author Jon T.
 * @author Ruben A.
 */
public final class clsMusica extends clsArticulo 
{
    /**
	 * Atributo del anyo de la musica
	 */
    private int int_anyo;

    /**
     * Constructor para inicializar el atributo
     */
    public clsMusica() 
    {
        super();
        this.int_anyo = 1900;
    }

    /**
     * Metodo que gestiona los atributos de la clase {@link clsMusica}
     * @param str_paramTitulo Parametro que guarda el titulo de la musica
     * @param en_paramGenero  Parametro que guarda el genero de la musica
     * @param int_paramPrecio Parametro que guarda el precio de la musica
     * @param str_paramCodigo Parametro que guarda el codigo de la musica
     * @param int_paramAnyo Parametro que guarda el anyo de la musica
     */
    public clsMusica(   String str_paramTitulo, enTipoGenero en_paramGenero, 
                        int int_paramPrecio, String str_paramCodigo,
                        int int_paramAnyo) 
    {
        super(  str_paramTitulo, en_paramGenero, int_paramPrecio, 
                str_paramCodigo);
        this.int_anyo = int_paramAnyo;
    }

    /**
     * Metodo que recoge el anyo de cada musica
     * @return Retorna el anyo de la musica
     */
    public int getAnyo() 
    {
        return this.int_anyo;
    }

    /**
     * Metodo que establece el anyo de cada musica
     * @param int_paramAnyo Parametro que guarda el anyo de la musica
     */
    public void setAnyo(int int_paramAnyo) 
    {
        this.int_anyo = int_paramAnyo;
    }

    @Override
    /**
     * Metodo toString, que convierte los atributos en una cadena para
     * poder mostrarlos en pantalla
     * @return retorna los atributos de la clase {@link clsMusica}
     */
    public String toString() 
    {
        String str_articulo;
        str_articulo = super.toString();
        return str_articulo + " Anyo: " + int_anyo;
    }

    @Override
    /**
	* Metodo que recoge las propiedades gestionadas por la clase itfProperty.
	* @param str_paramPropiedad Parametro que guarda la propiedad.
	*/
    public Object getObjectProperty(String str_paramPropiedad) 
    {
        switch ( str_paramPropiedad ) 
        {
            case clsConstantes.ANYO: return int_anyo;
            default : return super.getObjectProperty(str_paramPropiedad);
        }
    }
}