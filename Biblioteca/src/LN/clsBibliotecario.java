package LN;

import COMUN.*;
import COMUN.clsConstantes.en_Rol;

/**
 * Clase que contiene los metodos y los atributos que corresponden al rol
 * bibliotecario
 * 
 * @author Manel D.
 * @author Juan Mari D.
 * @author Jon T.
 * @author Ruben A.
 */
public final class clsBibliotecario extends clsUsuario 
{
  /**
   * Atributo que representa el DNI del bibliotecario
   */
  private String str_areaResponsabilidad;

  /**
   * Metodo que gestiona los atributos de clsBibliotecario
   * @param str_nombre nombre del Bibliotecario
   * @param str_email email del Bibliotecario
   * @param rol rol del Bibliotecario
   * @param int_Id identificador del Bibliotecario
   * @param str_areaResponsabilidad area de responsabilidad del Bibliotecario
   */
  public clsBibliotecario(String str_nombre, 
   String str_email, en_Rol rol, int int_Id, String str_areaResponsabilidad) 
   {
      super(str_nombre, str_email, rol, int_Id);
      this.str_areaResponsabilidad = str_areaResponsabilidad;
   }

  /**
   * Metodo que nos permite consultar el valor del ID del bibliotecario
   * @return Se retorna el area de responsbilidad del bibliotecario
   */
  public String getAreaResponsabilidad() 
  {
    return this.str_areaResponsabilidad;
  }

  /**
   * Metodo que nos permite cambiar el valor del ID del bibliotecario
   * @param str_areaResponsabilidad Area responsabilidad
   */
  public void setAreaResponsabilidad(String str_areaResponsabilidad) 
  {
    this.str_areaResponsabilidad = str_areaResponsabilidad;  
  }

  @Override
   /**
	* Metodo que recoge las propiedades gestionadas por la clase itfProperty.
	* @param str_paramPropiedad Parametro que guarda la propiedad.
	*/
    public Object getObjectProperty(String str_paramPropiedad) 
    {
       switch ( str_paramPropiedad ) 
         {
             case clsConstantes.AREA_RESPONSABILIDAD: 
                                          return str_areaResponsabilidad;
             default: return super.getObjectProperty(str_paramPropiedad);
         }
    }

   @Override
   /**
    * Metodo vacio para comparar Bibliotecarios (se hara más adelante)
    */
   public int compareTo(clsUsuario o) 
   {
      return 0;
   }
}