package LN;

import COMUN.*;
import COMUN.clsConstantes.en_Rol;
import EXCEPCIONES.clsPropiedadNoExistente;

/**
 * Clase que contiene los metodos que corresponden a la gestion del usuario
 * 
 * @author Manel D.
 * @author Juan Mari D.
 * @author Jon T.
 * @author Ruben A.
 */
public abstract class clsUsuario implements itfProperty, Comparable<clsUsuario> 
{

   /**
    * Atributo NOMBRE comun de los usuarios de la biblioteca
    */
   private String str_nombre;

   /**
    * Atributo EMAIL comun de los usuarios de la biblioteca
    */
   private String str_email;

   /**
    * Atributo ROL comun de los usuarios de la biblioteca
    */
   private en_Rol en_rolUsario;

   /**
    * Atributo identificativo comun de los usuarios
    */
   private int int_Id;

   /**
    * Metodo que recoge el rol de cada usuario
    * @return Retorna el rol de cada usuario
    */
   public en_Rol getRol() 
   {
      return en_rolUsario;
   }

   /**
    * Constructor para inicializar cada atributo
    */
   public clsUsuario() 
   {
      this.str_nombre = " ";
      this.str_email = " ";
      en_rolUsario = en_Rol.en_rolInvalid;
      this.int_Id = 0;
   }

   /**
    * Metodo que gestiona los atributos de la clase {@link clsUsuario}
    * @param str_nombre   Parametro que guarda el Nombre del usuario
    * @param str_email    Parametro que guarda el Email del usuario
    * @param en_rolUsario Parametro que guarda el Rol del usuario
    * @param int_Id       Parametro que guarda el ID del usuario
    */
   public clsUsuario(String str_nombre, String str_email, en_Rol en_rolUsario, 
                     int int_Id) 
   {
      this.str_nombre = str_nombre;
      this.str_email = str_email;
      this.en_rolUsario = en_rolUsario;
      this.int_Id = int_Id;
   }

   /**
    * Metodo que ermite consultar el nombre del usuario
    * @return nombre del usuario
    */
   public String getNombre() 
   {
      return this.str_nombre;
   }

   /**
    * Metodo que ermite consultar el Email del usuario
    * @return email del usuario
    */
   public String getEmail() 
   {
      return this.str_email;
   }

   /**
    * Metodo para consultar el Rol del usuario
    * @return Rol del usuario
    */
   public en_Rol getEn_rolUsario() 
   {
      return en_rolUsario;
   }

   /**
    * Metodo que ermite consultar el ID del usuario
    * @return id del usuario
    */
   public int getId() 
   {
      return this.int_Id;
   }

   /**
    * Metodo que establece el nombre de cada usuario
    * @param str_nombre nombre del usuario
    */
   public void setNombre(String str_nombre) 
   {
      this.str_nombre = str_nombre;
   }

   /**
    * Metodo que establece el email de cada usuario
    * @param str_email email del usuario
    */
   public void setEmail(String str_email) 
   {
      this.str_email = str_email;
   }

   /**
    * Metodo que establece el id de cada usuario
    * @param int_Id Id del usuario
    */
   public void setId(int int_Id) 
   {
      this.int_Id = int_Id;
   }

   @Override
   /**
    * Metodo hashCode de la clase clsUsuario
    */
   public int hashCode() 
   {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((str_nombre == null) ?0:str_nombre.hashCode());
      return result;
   }

   @Override
   /**
    * Metodo equals de la clase clsUsuario
    */
   public boolean equals(Object obj) 
   {
      // si el dato que estas comparando es del mismo tipo
      if (obj instanceof clsRegistrado) 
      {
         // Conversion de tipos, procesar el objeto o como clsJugador
         clsRegistrado param_registrado = (clsRegistrado) obj;

         if (param_registrado.getNombre().equals(str_nombre)) 
         {
            return true;
         } 
         else 
         {
            return false;
         }
      }
      // no son datos del mismo tipo
      else 
      {
         return false;
      }
   }

   /**
    * Metodo que recoge las propiedades gestionadas por la clase itfProperty.
    * @param str_paramPropiedad Parametro que guarda la propiedad.
    */
   public Object getObjectProperty(String str_paramPropiedad) 
   {
      switch (str_paramPropiedad) 
      {
         case clsConstantes.NOMBRE:
            return str_nombre;

         case clsConstantes.EMAIL:
            return str_email;

         case clsConstantes.ROL:
            return en_rolUsario;

         case clsConstantes.ID:
            return int_Id;

         default:
            throw new clsPropiedadNoExistente(str_paramPropiedad);
      }
   }

   @Override
   /**
    * Compara los usuarios por el nombre
    */
   public int compareTo(clsUsuario obj_Usuario) 
   {
      return this.str_nombre.compareTo(obj_Usuario.str_nombre);
   }
}
