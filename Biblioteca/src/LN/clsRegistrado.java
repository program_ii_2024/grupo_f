package LN;

import COMUN.*;
import COMUN.clsConstantes.en_Rol;

/**
 * Clase que contiene los metodos que corresponden a la gestion del rol 
 * registrado
 * @author Manel D.
 * @author Juan Mari D.
 * @author Jon T.
 * @author Ruben A.
 */
public class clsRegistrado extends clsUsuario
{
     
   /**
    * atributo numero de telefono del Registrado
    */
  private int int_telefonoRegistrado;

 /**
  *  Metodo que gestiona los atributos de la clase clsRegistrado
  * @param int_telefonoRegistrado telefono del registrado
  * @param str_nombre nombre del usuario registrado
  * @param str_email email del usuario registrado
  * @param rol rol del usuario
  * @param int_Id id del usuario
  */
  public clsRegistrado(int int_telefonoRegistrado, String str_nombre, 
                       String str_email, en_Rol rol, int int_Id) 
   {
      super(str_nombre, str_email, rol, int_Id);
      this.int_telefonoRegistrado = int_telefonoRegistrado;
   }
   
   /**
    * Getter de clsRegistrado
    * @return el telefono del usuario registrado
    */
   public int getTelefonoRegistrado() 
   {
      return this.int_telefonoRegistrado;
   }
   /**
    * Setter de clsRegistrado
    * @param int_telefonoRegistrado telefono del usuario registrado
    */
   public void setTelefonoRegistrado(int int_telefonoRegistrado) 
   {
      this.int_telefonoRegistrado = int_telefonoRegistrado;
   } 

  @Override
   /**
	* Metodo que recoge las propiedades gestionadas por la clase itfProperty.
	* @param str_paramPropiedad Parametro que guarda la propiedad.
	*/
   public Object getObjectProperty(String str_paramPropiedad) 
   {
      switch ( str_paramPropiedad ) 
        {
          case clsConstantes.TELEFONO_REGISTRADO: return int_telefonoRegistrado;  
          default: return super.getObjectProperty(str_paramPropiedad);
        }
   }

   @Override
   /**
    * Metodo para comparar usuarios (se usara mas adelane)
    */
   public int compareTo(clsUsuario o) 
   {
      return 0;
   }
}
