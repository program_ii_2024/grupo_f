package LN;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import COMUN.*;
import COMUN.clsConstantes.en_Rol;
import EXCEPCIONES.clsArticuloRepetido;
import EXCEPCIONES.clsCodigoIncorrecto;
import LD.clsGestorLD;

/**
 * Clase que gestiona las demas clases que pertenecen al paquete LN
 * @author Manel D.
 * @author Juan Mari D.
 * @author Jon T.
 * @author Ruben A.
 */
public class clsGestorLN 
{
    /**
     * TreeSet para guardar los articulos
     */
    private TreeSet<clsArticulo> set_articulos;

    /**
     * HashMap para guardar los usuarios
     */
    private HashMap<Integer, clsUsuario> map_usuarios;

    /**
     * atributo quye encierra un objeto de tipo clsGestorLN
     */
    private clsGestorLD gld;

    /**
     * Constructor del clsGestorLN
     */
    public clsGestorLN() 
    {
        gld = new clsGestorLD();
        set_articulos = new TreeSet<>();

        this.vo_recuperarMusica();

        new clsComparadorArticulos(true);
        vo_inicializar_articulos();
        map_usuarios = new HashMap<>();
        vo_inicializarUsuarios();
    }

    /**
     * Metodo para inicializar los usuarios
     */
    private void vo_inicializarUsuarios() 
    {
        clsRegistrado u = new clsRegistrado(6789345,
                "Jon", "jon@gmail.com", en_Rol.en_rolWrite,
                888);
        map_usuarios.put(u.getId(), u);

        clsBibliotecario b = new clsBibliotecario("Ekaitz",
                "ekaitz@gmail.com", en_Rol.en_rolAdmin, 777,
                "Seccion Adulta");
        map_usuarios.put(b.getId(), b);

    }

    /**
     * Metodo para inicializar los articulos
     */
    private void vo_inicializar_articulos() 
    {
        set_articulos.add(new clsPelicula("Cars",
                enTipoGenero.en_accion, 10,
                 "PELI1",
                "Director de Disney"));
        set_articulos.add(new clsLibro("Harry Potter",
                enTipoGenero.en_historico, 50, 
                "LIBR1",
                "Elkar"));
        set_articulos.add(new clsMusica("YHLQMDLG",
                enTipoGenero.en_romantico, 29, 
                "MUS1",
                2020));
        set_articulos.add(new clsMusica("Saturno",
                enTipoGenero.en_accion, 33, 
                "MUS2",
                2022));
    }

    /**
     * Metodo para modificar el email de usuario
     * @param str_paramNombre Nombre
     * @param str_paramEmail Email
     * @return True o false
     */
    public boolean bln_modificarEmailUsuario(String str_paramNombre, 
                                             String str_paramEmail) 
    {
        clsUsuario obj_user = obj_buscarUsuario(str_paramNombre);
        clsUsuario obj_newUser = new clsRegistrado(0, 
        obj_user.getNombre(), str_paramEmail, obj_user.getRol(),
        obj_user.getId());

        map_usuarios.remove(obj_user.getId(), obj_user);
        map_usuarios.put(obj_newUser.getId(), obj_newUser);

        if (map_usuarios.containsKey(obj_newUser.getId())) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Metodo para modificar el nombre de usuario
     * @param str_paramNombre Nombre
     * @param str_paramNewNombre Nuevo nombre
     * @return True o false
     */
    public boolean bln_modificarNombreUsuario(String str_paramNombre, 
                                              String str_paramNewNombre) 
    {
        clsUsuario obj_user = obj_buscarUsuario(str_paramNombre);

        clsUsuario obj_newUser = new clsRegistrado(0, 
        str_paramNewNombre, obj_user.getEmail(), obj_user.getRol(),
        obj_user.getId());

        map_usuarios.remove(obj_user.getId(), obj_user);
        map_usuarios.put(obj_newUser.getId(), obj_newUser);

        if (map_usuarios.containsKey(obj_newUser.getId())) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Metodo para modificar el id de usuario
     * @param str_paramNombre Nombre
     * @param int_paramId Id
     * @return True o false
     */
    public boolean bln_modificarIdUsuario(String str_paramNombre, 
                                          int int_paramId) 
    {
        clsUsuario obj_user = obj_buscarUsuario(str_paramNombre);

        clsUsuario obj_newUser = new clsRegistrado(0, 
        obj_user.getNombre(), obj_user.getEmail(), obj_user.getRol(),
        int_paramId);

        map_usuarios.remove(obj_user.getId(), obj_user);
        map_usuarios.put(obj_newUser.getId(), obj_newUser);

        if (map_usuarios.containsKey(obj_newUser.getId())) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Metodo para comprobar el usuario
     * @param str_paramNombre Nombre
     * @param int_paramId Id
     * @return True o false
     */
    public boolean bln_comprobarUsuario(String str_paramNombre,
            int int_paramId) 
            {
        clsUsuario user = obj_buscarUsuario(str_paramNombre);
        if (user == null) 
        {
            return false;
        }
        if (user.getNombre().equalsIgnoreCase(str_paramNombre)) 
        {
            if (user.getId() == int_paramId) 
            {
                return true;
            } 
            else 
            {
                return false;
            }
        }
        return false;
    }

    /**
     * Metodo que inserta Registrados
     * @param nombre nombre del registrado
     * @param email  email del registrado
     * @param rol    rol del registrado
     * @return boolean seun el resultado de las operaciones
     */
    public boolean bln_insertarRegistrado(String nombre, String email, 
                                          en_Rol rol) 
    {
        clsUsuario obj_usuario;
        obj_usuario = new clsRegistrado(0,
                nombre, email, rol, 0);

        if (map_usuarios.containsKey(obj_usuario.getId())) 
        {
            return false;
        } 
        else 
        {
            map_usuarios.put(obj_usuario.getId(), obj_usuario);
            return true;
        }
    }

    /**
     * Metodo que borra los usuarios registrados
     * @param str_nombre nombre del usuario consultado
     * @return boolean segun el resultado de las operaciones
     */
    public boolean bln_borrarRegistrado(String str_nombre) 
    {
        clsRegistrado usuarioABorrar = 
        new clsRegistrado(0, str_nombre, str_nombre,
                          null, 0);
        return map_usuarios.remove(usuarioABorrar.getId()) != null;
    }

    /**
     * Metodo que confirma el usuario
     * @param str_nombre nombre del usuario consultado
     * @return tre / false segun sea o no Registrado
     */
    public boolean bln_esUsuarioRegistrado(String str_nombre) 
    {
        for (clsUsuario u : this.map_usuarios.values()) 
        {
            if (u instanceof clsRegistrado) 
            {
                if (((clsRegistrado) u).getNombre().equals(str_nombre))
                    return true;
            }
        }
        return false;
    }

    /**
     * Se usa para ver si un usuario es de tipo clsBibliotecario
     * @param str_nombre Nombre del usuario consultado
     * @return valor true / false segun sea o no Biblioptecario
     */
    public boolean bln_esUsuarioBibliotecario(String str_nombre) 
    {

        for (clsUsuario u : this.map_usuarios.values()) 
        {
            if (u instanceof clsBibliotecario) 
            {
                if (((clsBibliotecario) u).getNombre().equals(str_nombre))
                    return true;
            }
        }
        return false;
    }

    /**
     * Metodo que busca usuarios
     * @param str_nombre
     * @return el usuario buscado o null
     */
    private clsUsuario obj_buscarUsuario(String str_nombre) 
    {
        Iterator<clsUsuario> it = map_usuarios.values().iterator();
        while (it.hasNext()) 
        {
            clsUsuario u = it.next();
            if (u.getNombre().equals(str_nombre)) 
            {
                return u;
            }
        }
        return null;
    }

    /**
     * Funcion usada para devolver una estructura de articulos
     * @param str_paramCodigo codigo del articulo
     * @return el clsArticulo buscado
     */
    private clsArticulo obj_buscarArticulo(String str_paramCodigo) 
    {
        Iterator<clsArticulo> it = set_articulos.iterator();
        while (it.hasNext()) 
        {
            clsArticulo u = it.next();
            if (u.getCodigo().equals(str_paramCodigo)) 
            {
                return u;
            }
        }
        return null;
    }

    /**
     * Metodo que recupera la información de los usuarios
     * @param str_nombre nombre del usuario
     * @return información del usuario
     */
    public String recuperarInformacionUsuario(String str_nombre) 
    {
        clsUsuario obj_usuario;
        String texto = "";

        obj_usuario = obj_buscarUsuario(str_nombre);

        texto = obj_usuario.getNombre() + " - " + obj_usuario.getEmail();

        return texto;
    }

    /**
     * ArrayList que guarda la informacion recuperada de los usuarios
     * @return arrayList con la informacion de los usuarios
     */
    public ArrayList<String> recuperarInformacionUsuarios() 
    {

        ArrayList<String> infoUsuarios = new ArrayList<>();
        String info;
        for (clsUsuario objUsuario : map_usuarios.values()) 
        {
            info = objUsuario.getNombre() + "-" + objUsuario.getEmail();
            infoUsuarios.add(info);
        }
        return infoUsuarios;
    }

    /**
     * Metodo que comprueba los roles de los usuarios
     * @param str_usuario nombre del usuario
     * @return el Rol del usuario
     */
    public en_Rol comprobarRol(String str_usuario) 
    {

        clsUsuario u = obj_buscarUsuario(str_usuario);

        return u.getRol();
    }

    /**
     * Metodo para insertar Libros
     * @param str_paramTitulo    Parametro que guarda el titulo del libro
     * @param en_paramGenero     Parametro que guarda el genero del libro
     * @param int_paramPrecio    Parametro que guarda el precio del libro
     * @param str_paramCodigo    Parametro que guarda el codigo del libro
     * @param str_paramEditorial Parametro que guarda la editorial del libro
     * @throws clsArticuloRepetido La excepcion
     */
    public void vo_InsertarLibro(String str_paramTitulo,
            enTipoGenero en_paramGenero,
            int int_paramPrecio,
            String str_paramCodigo,
            String str_paramEditorial) throws clsArticuloRepetido
    {
        clsLibro obj_ClsLibro = new clsLibro(str_paramTitulo,
                en_paramGenero,
                int_paramPrecio,
                str_paramCodigo,
                str_paramEditorial);
        if (set_articulos.contains(obj_ClsLibro)) 
        {
            throw new clsArticuloRepetido(str_paramCodigo);
        } 
        else 
        {
            set_articulos.add(obj_ClsLibro);
        }
    }

    /**
     * Metodo para recuperar musica de la base de datos
     */
    private void vo_recuperarMusica()
    {

        gld.conectar();

        ResultSet rs = gld.rs_recuperarMusica();

        try 
        {
            while(rs.next())
            {
                enTipoGenero g;
                String t = rs.getString("TITULO");
                int p = rs.getInt("PRECIO");
                String c = rs.getString("CODIGO");
                int a = rs.getInt("ANYO");

                switch (rs.getInt("GENERO")) 
                {
                    case 1:
                        g = enTipoGenero.en_historico;
                        break;
                    case 2:
                        g = enTipoGenero.en_terror;
                        break;
                    case 3:
                        g = enTipoGenero.en_accion;
                        break;
                    default:
                        g = enTipoGenero.en_romantico;
                        break;
                }

                clsMusica e = new clsMusica(t, g, p, c, a );

                set_articulos.add(e);

            }

            gld.desconectar();
        } 
        catch(SQLException e) 
        {
            e.printStackTrace();
        }
    }

    /**
     * Recuperar articulos
     * @return una estructura array con los Articulos buscados
     */
    public ArrayList<itfProperty> obj_recuperarArticulos() 
    {
        ArrayList<itfProperty> arrl_articulos = new ArrayList<>();

        for (clsArticulo obj : set_articulos) 
        {
            arrl_articulos.add(obj);
        }
        return arrl_articulos;
    }

    /**
     * Metodo para borrar los libros
     * @param str_codigo codigo del libro
     * @return true / false segun el resultado de las operaciones
     */
    public boolean bln_borrarLibro(String str_codigo) 
    {
        clsLibro LB = new clsLibro("",
                null, 0, 
                str_codigo, "");
        return set_articulos.remove(LB);
    }

    /**
     * Metodo para insertar la musica
     * @param str_paramTitulo Parametro que guarda el titulo de la musica
     * @param en_paramGenero  Parametro que guarda el genero de la musica
     * @param int_paramPrecio Parametro que guarda el precio de la musica
     * @param str_paramCodigo Parametro que guarda el codigo de la musica
     * @param int_paramAnyo   Parametro que guarda el anyo de la musica
     * @throws clsArticuloRepetido La excepcion
     */
    public void vo_InsertarMusica(String str_paramTitulo,
            enTipoGenero en_paramGenero,
            int int_paramPrecio,
            String str_paramCodigo,
            int int_paramAnyo) throws clsArticuloRepetido
    {
        clsMusica obj_clsMusica = new clsMusica(str_paramTitulo,
                en_paramGenero,
                int_paramPrecio,
                str_paramCodigo,
                int_paramAnyo);
        if (set_articulos.contains(obj_clsMusica)) 
        {
            throw new clsArticuloRepetido(str_paramCodigo);
        } 
        else 
        {
            gld.vo_insertarMusica(str_paramTitulo, en_paramGenero, 
            int_paramPrecio, str_paramCodigo, int_paramAnyo);

            set_articulos.add(obj_clsMusica);
        }
    }

    /**
     * Metodo para borrar la musica
     * @param str_codigo codigo de la musica
     * @return true / false segun las operaciones realizadas
     */
    public boolean bln_borrarMusica(String str_codigo) 
    {
        clsMusica MU = new clsMusica("",
                null, 0, 
                str_codigo, 0);

        gld.vo_borrarMusica(str_codigo);

        return set_articulos.remove(MU);
    }

    /**
     * Metodo para insertar las peliculas
     * @param str_paramTitulo   Parametro que guarda el titulo de la pelicula
     * @param en_paramGenero    Parametro que guarda el genero de la pelicula
     * @param int_paramPrecio   Parametro que guarda el precio de la pelicula
     * @param str_paramCodigo   Parametro que guarda el codigo de la pelicula
     * @param str_paramDirector Parametro que guarda el director de la pelicula
     * @throws clsArticuloRepetido La excepcion
     */
    public void vo_InsertarPelicula(String str_paramTitulo,
            enTipoGenero en_paramGenero,
            int int_paramPrecio,
            String str_paramCodigo,
            String str_paramDirector) throws clsArticuloRepetido
    {
        clsPelicula obj_clsPelicula = new clsPelicula(str_paramTitulo,
                en_paramGenero,
                int_paramPrecio,
                str_paramCodigo,
                str_paramDirector);
        if (set_articulos.contains(obj_clsPelicula)) 
        {
            throw new clsArticuloRepetido(str_paramCodigo);
        } 
        else 
        {
            set_articulos.add(obj_clsPelicula);
        }
    }

    /**
     * Metodo para borrar las peliculas
     * @param str_codigo codigo de la pelicula
     * @return true / false segun las operaciones realizadas
     */
    public boolean bln_borrarPelicula(String str_codigo) 
    {
        clsPelicula PE = new clsPelicula("",
        null, 0, str_codigo, "");
        return set_articulos.remove(PE);
    }

    /**
     * Funcion que devuelve un listado de peliculas buscadas
     * @return listado de peliculas consultadas
     */
    public ArrayList<itfProperty> obj_recuperarPeliculas() 
    {
        throw new UnsupportedOperationException(
                "Unimplemented method 'obj_recuperarPeliculas'");
    }

    /**
     * Metodo para comprobar si el objeto es un libro
     * @param obj_paramLibro objeto que recibe para compararlo con la clase
     * @return true / false segun las operaciones realizadas
     */
    public boolean bln_comprobarLibro(itfProperty obj_paramLibro) 
    {
        boolean bln_respuesta;
        if (obj_paramLibro instanceof clsLibro) 
        {
            bln_respuesta = true;
        } 
        else 
        {
            bln_respuesta = false;
        }
        return bln_respuesta;
    }

    /**
     * Metodo para comprobar si el objeto es musica
     * @param obj_paramMusica objeto que recibe para compararlo con la clase
     * @return Devuelve true en caso de que el objeto que recibe sea musica
     */
    public boolean bln_comprobarMusica(itfProperty obj_paramMusica) 
    {
        boolean bln_respuesta;
        if (obj_paramMusica instanceof clsMusica) 
        {
            bln_respuesta = true;
        } 
        else 
        {
            bln_respuesta = false;
        }
        return bln_respuesta;
    }

    /**
     * Metodo para comprobar si el objeto es una pelicula
     * @param obj_paramPelicula objeto que recibe para compararlo con la clase
     * @return Devuelve true en caso de que el objeto que recibe sea una pelicula
     */
    public boolean bln_comprobarPelicula(itfProperty obj_paramPelicula) 
    {
        boolean bln_respuesta;
        if (obj_paramPelicula instanceof clsPelicula) 
        {
            bln_respuesta = true;
        } 
        else 
        {
            bln_respuesta = false;
        }
        return bln_respuesta;
    }

    /**
     * Funcion que modifica el Genero de un libro
     * @param str_paramCodigo codigo del libro a modificar
     * @param en_paramGenero nuevo genero del libro
     * @return true / false segun las operaciones realizadas
     */
    public boolean bln_ModificarGeneroLibro(String str_paramCodigo, 
                                            enTipoGenero en_paramGenero) 
    {
        clsLibro obj_libro = (clsLibro) obj_buscarArticulo(str_paramCodigo);
        clsLibro obj_newLibro = new clsLibro(obj_libro.getTitulo(), 
        en_paramGenero, obj_libro.getPrecio(), str_paramCodigo, 
        obj_libro.getEditorial());

        set_articulos.remove(obj_libro);
        set_articulos.add(obj_newLibro);

        if (set_articulos.contains(obj_newLibro) && 
        obj_newLibro.getGenero() == en_paramGenero) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Funcion que modifica el Titulo de un libro
     * @param str_paramCodigo codigo del libro a modificar
     * @param str_paramTitulo nuevo titulo 
     * @return true / false segun las operaciones realizadas
     */
    public boolean bln_ModificarTituloLibro(String str_paramCodigo,
                                            String str_paramTitulo) 
    {
        clsLibro obj_libro = (clsLibro) obj_buscarArticulo(str_paramCodigo);
        clsLibro obj_newLibro = new clsLibro(str_paramTitulo, 
        obj_libro.getGenero(), obj_libro.getPrecio(), str_paramCodigo, 
        obj_libro.getEditorial());

        set_articulos.remove(obj_libro);
        set_articulos.add(obj_newLibro);

        if (set_articulos.contains(obj_newLibro) && 
        obj_newLibro.getTitulo() == str_paramTitulo) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Funcion que modifica el codigo de un libro
     * @param str_paramCodigo codigo del libro buscado
     * @param str_paramNewCodigo nuevo codigo
     * @return true / false segun las operaciones realizadas
     */
    public boolean bln_ModificarCodigoLibro(String str_paramCodigo, 
                                            String str_paramNewCodigo) 
    {
        clsLibro obj_libro = (clsLibro) obj_buscarArticulo(str_paramCodigo);
        clsLibro obj_newLibro = new clsLibro(obj_libro.getTitulo(), 
        obj_libro.getGenero(), obj_libro.getPrecio(), str_paramNewCodigo,
         obj_libro.getEditorial());

        set_articulos.remove(obj_libro);
        set_articulos.add(obj_newLibro);

        if (set_articulos.contains(obj_newLibro) && 
        obj_newLibro.getCodigo() == str_paramNewCodigo) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Funcion que modifica el precio de un libro
     * @param str_paramCodigo codigo del libro buscado
     * @param int_paramNewPrecio nuevo precio
     * @return true / false segun las operaciones realizadas
     */
    public boolean bln_ModificarPrecioLibro(String str_paramCodigo, 
                                            int int_paramNewPrecio) 
    {
        clsLibro obj_libro = (clsLibro) obj_buscarArticulo(str_paramCodigo);
        clsLibro obj_newLibro = new clsLibro(obj_libro.getTitulo(), 
        obj_libro.getGenero(), int_paramNewPrecio, obj_libro.getCodigo(),
         obj_libro.getEditorial());

        set_articulos.remove(obj_libro);
        set_articulos.add(obj_newLibro);

        if (set_articulos.contains(obj_newLibro) &&
         obj_newLibro.getPrecio() == int_paramNewPrecio)
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * funcion que modifica el editorial de un libro
     * @param str_paramCodigo codigo del libro buscado
     * @param str_paramNewEditorial nueva editorial
     * @return true / false segun las operaciones realizadas
     */ 
    public boolean bln_ModificarEditorialLibro(String str_paramCodigo, 
                                               String str_paramNewEditorial) 
    {
        clsLibro obj_libro = (clsLibro) obj_buscarArticulo(str_paramCodigo);
        clsLibro obj_newLibro = new clsLibro(obj_libro.getTitulo(), 
        obj_libro.getGenero(), obj_libro.getPrecio(), obj_libro.getCodigo(), 
        str_paramNewEditorial);

        set_articulos.remove(obj_libro);
        set_articulos.add(obj_newLibro);

        if (set_articulos.contains(obj_newLibro) && 
        obj_newLibro.getEditorial() == str_paramNewEditorial) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Esta funcion sirve para Modificar el Codigo de los elementos clsMusica
     * @param str_paramCodigo codigo de la musica a modificar
     * @param str_paramNewCodigo nuevo Codigo a modificar
     * @return true / false segun las operaciones realizadas
     */
    public boolean bln_ModificarCodigoMusica(String str_paramCodigo, 
                                            String str_paramNewCodigo) 
    {
        clsMusica obj_musica = (clsMusica) obj_buscarArticulo(str_paramCodigo);
        clsMusica obj_newMusica = new clsMusica(obj_musica.getTitulo(),
        obj_musica.getGenero(),obj_musica.getPrecio(),str_paramNewCodigo,
        obj_musica.getAnyo());

        set_articulos.remove(obj_musica);
        set_articulos.add(obj_newMusica);

        if (set_articulos.contains(obj_newMusica) && 
        obj_newMusica.getCodigo() == str_paramNewCodigo) 
        {
            gld.vo_modificarMusica(obj_newMusica.getTitulo(),
            obj_newMusica.getGenero(),obj_newMusica.getPrecio(),
            str_paramNewCodigo, obj_newMusica.getAnyo());

            return true;
        } 
        else 
        {
            return false;
        }
    }
    
    /**
     * Esta funcion sirve para Modificar el Titulo de los elementos ClsMusica
     * @param str_paramCodigo codigo de la musica a modificar
     * @param str_paramTitulo nuevo titulo a modificar
     * @return true / false segun las operaciones realizadas
     */
    public boolean bln_ModificarTituloMusica(String str_paramCodigo,
                                            String str_paramTitulo) 
    {
        clsMusica obj_musica = (clsMusica) obj_buscarArticulo(str_paramCodigo);
        clsMusica obj_newMusica = new clsMusica(str_paramTitulo, 
        obj_musica.getGenero(),obj_musica.getPrecio(),obj_musica.getCodigo(),
        obj_musica.getAnyo());

        set_articulos.remove(obj_musica);
        set_articulos.add(obj_newMusica);

        if (set_articulos.contains(obj_newMusica) && 
        obj_newMusica.getTitulo() == str_paramTitulo) 
        {
            gld.vo_modificarMusica(str_paramTitulo, obj_newMusica.getGenero(),
            obj_newMusica.getPrecio(),obj_newMusica.getCodigo(),
            obj_newMusica.getAnyo());
            return true;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Funcion que permite modificar el Genero de Musica
     * @param str_paramCodigo Codigo de la musica buscada
     * @param en_paramGenero Nuevo genero a modificar
     * @return true / false segun las operaciones realizadas
     */
    public boolean bln_ModificarGeneroMusica(String str_paramCodigo, 
                                            enTipoGenero en_paramGenero) 
    {
        clsMusica obj_musica = (clsMusica) obj_buscarArticulo(str_paramCodigo);
        clsMusica obj_newMusica = new clsMusica(obj_musica.getTitulo(),
        en_paramGenero,obj_musica.getPrecio(),obj_musica.getCodigo(),
        obj_musica.getAnyo());

        set_articulos.remove(obj_musica);
        set_articulos.add(obj_newMusica);

        if (set_articulos.contains(obj_newMusica) && 
        obj_newMusica.getGenero() == en_paramGenero) 
        {
            gld.vo_modificarMusica(obj_newMusica.getTitulo(), en_paramGenero, 
            obj_newMusica.getPrecio(), obj_newMusica.getCodigo(), 
            obj_newMusica.getAnyo());
            return true;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Funcion que premite modificar el Precio de la musica
     * @param str_paramCodigo Codigo de la musica buscada
     * @param int_paramNewPrecio Nuevo precio a modificar
     * @return true / false segun las operaciones realizadas
     */
    public boolean bln_ModificarPrecioMusica(String str_paramCodigo, 
                                            int int_paramNewPrecio) 
    {
        clsMusica obj_musica = (clsMusica) obj_buscarArticulo(str_paramCodigo);
        clsMusica obj_newMusica = new clsMusica(obj_musica.getTitulo(),
        obj_musica.getGenero(),int_paramNewPrecio,obj_musica.getCodigo(),
        obj_musica.getAnyo());

        set_articulos.remove(obj_musica);
        set_articulos.add(obj_newMusica);

        if (set_articulos.contains(obj_newMusica) &&
         obj_newMusica.getPrecio() == int_paramNewPrecio)
        {
            gld.vo_modificarMusica(obj_newMusica.getTitulo(), 
            obj_newMusica.getGenero(), int_paramNewPrecio, 
            obj_newMusica.getCodigo(), obj_newMusica.getAnyo());
            return true;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Funcion que permite la modificacion de el Anyo de la Musica
     * @param str_paramCodigo Codigo de la Musica a modificar
     * @param int_paramNewAnyo Nuevo Anyo a modificar
     * @return true / false segun las operaciones realizadas
     */
    public boolean bln_ModificarAnyoMusica(String str_paramCodigo, 
                                            int int_paramNewAnyo) 
    {
        clsMusica obj_musica = (clsMusica) obj_buscarArticulo(str_paramCodigo);
        clsMusica obj_newMusica = new clsMusica(obj_musica.getTitulo(),
        obj_musica.getGenero(),obj_musica.getPrecio(),obj_musica.getCodigo(),
        int_paramNewAnyo);

        set_articulos.remove(obj_musica);
        set_articulos.add(obj_newMusica);

        if (set_articulos.contains(obj_newMusica) &&
         obj_newMusica.getAnyo() == int_paramNewAnyo)
        {
            gld.vo_modificarMusica(obj_newMusica.getTitulo(), 
            obj_newMusica.getGenero(), obj_newMusica.getPrecio(), 
            obj_newMusica.getCodigo(), int_paramNewAnyo);
            return true;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Funcion que permite modificar El codigo de una Pelicula
     * @param str_paramCodigo Codigo de la pelicula buscada
     * @param str_paramNewCodigo Nuevo codigo a modificar
     * @return true / false segun las operaciones realizadas
     */
    public boolean bln_ModificarCodigoPelicula(String str_paramCodigo, 
                                            String str_paramNewCodigo) 
    {
        clsPelicula obj_pelicula = (clsPelicula) obj_buscarArticulo(str_paramCodigo);
        clsPelicula obj_newPelicula = new clsPelicula(obj_pelicula.getTitulo(),
        obj_pelicula.getGenero(),obj_pelicula.getPrecio(),str_paramNewCodigo,
        obj_pelicula.getDirector());

        set_articulos.remove(obj_pelicula);
        set_articulos.add(obj_newPelicula);

        if (set_articulos.contains(obj_newPelicula) && 
        obj_newPelicula.getCodigo() == str_paramNewCodigo) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Funcion que premite la modificacion del Tituklo de las peliculas
     * @param str_paramCodigo Codigo de la Pelicula buscada
     * @param str_paramNewTitulo Nuevo titulo a modificar
     * @return true / false segun las operaciones realizadas
     */
    public boolean bln_ModificarTituloPelicula(String str_paramCodigo, 
                                            String str_paramNewTitulo) 
    {
        clsPelicula obj_pelicula = (clsPelicula) obj_buscarArticulo(str_paramCodigo);
        clsPelicula obj_newPelicula = new clsPelicula(str_paramNewTitulo,
        obj_pelicula.getGenero(),obj_pelicula.getPrecio(),
        obj_pelicula.getCodigo(), obj_pelicula.getDirector());

        set_articulos.remove(obj_pelicula);
        set_articulos.add(obj_newPelicula);

        if (set_articulos.contains(obj_newPelicula) && 
        obj_newPelicula.getTitulo() == str_paramNewTitulo) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Funcion que premite ela modificacion del Genero de una pelicula
     * @param str_paramCodigo Codigo de la pelicula buscada
     * @param en_paramNewGenero Nuevo genero a modificar
     * @return true / false segun las operaciones realizadas
     */
    public boolean bln_ModificarGeneroPelicula(String str_paramCodigo, 
                                               enTipoGenero en_paramNewGenero) 
    {
        clsPelicula obj_pelicula = 
        (clsPelicula) obj_buscarArticulo(str_paramCodigo);
        clsPelicula obj_newPelicula = new clsPelicula(obj_pelicula.getTitulo(),
        en_paramNewGenero,obj_pelicula.getPrecio(),obj_pelicula.getCodigo(),
        obj_pelicula.getDirector());

        set_articulos.remove(obj_pelicula);
        set_articulos.add(obj_newPelicula);

        if (set_articulos.contains(obj_newPelicula) && 
        obj_newPelicula.getGenero() == en_paramNewGenero) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Funcion que permite la modificacion del precio de la pelicula
     * @param str_paramCodigo Codigo de la pelicula buscada
     * @param int_paramNewPrecio Nuevo precio a modificar
     * @return true / false segun las operaciones realizadas
     */ 
    public boolean bln_ModificarPrecioPelicula(String str_paramCodigo, 
                                               int int_paramNewPrecio) 
    {
        clsPelicula obj_pelicula = 
        (clsPelicula) obj_buscarArticulo(str_paramCodigo);
        clsPelicula obj_newPelicula = new clsPelicula(obj_pelicula.getTitulo(),
        obj_pelicula.getGenero(),int_paramNewPrecio,obj_pelicula.getCodigo(),
        obj_pelicula.getDirector());

        set_articulos.remove(obj_pelicula);
        set_articulos.add(obj_newPelicula);

        if (set_articulos.contains(obj_newPelicula) && 
        obj_newPelicula.getPrecio() == int_paramNewPrecio) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Funcion que permite la modificacion del Director de una pelicula
     * @param str_paramCodigo Codigo de la pelicula buscada
     * @param str_paramNewDirector Nuevo Director a modificar
     * @return true / false segun las operaciones realizadas
     */
    public boolean bln_ModificarDirectorPelicula(String str_paramCodigo, 
                                               String str_paramNewDirector) 
    {
        clsPelicula obj_pelicula = 
        (clsPelicula) obj_buscarArticulo(str_paramCodigo);
        clsPelicula obj_newPelicula = new clsPelicula(obj_pelicula.getTitulo(),
        obj_pelicula.getGenero(),obj_pelicula.getPrecio(),
        obj_pelicula.getCodigo(), str_paramNewDirector);

        set_articulos.remove(obj_pelicula);
        set_articulos.add(obj_newPelicula);

        if (set_articulos.contains(obj_newPelicula) && 
        obj_newPelicula.getDirector() == str_paramNewDirector) 
        {
            return true;
        } 
        else 
        {
            return false;
        }
    }

    /**
     * Metodo para buscar articulo
     * @param str_paramCodigo Codigo
     * @return True o false
     * @throws clsCodigoIncorrecto La excepcion
     */
    public boolean bln_buscarArticulo(String str_paramCodigo) 
    throws clsCodigoIncorrecto
    {
        Iterator<clsArticulo> it = set_articulos.iterator();
        while (it.hasNext()) 
        {
            clsArticulo u = it.next();
            if (u.getCodigo().equals(str_paramCodigo)) 
            {
                return true;
            }
        }

        throw new clsCodigoIncorrecto(str_paramCodigo);
    }

}
