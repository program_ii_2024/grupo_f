package LN;

import COMUN.*;
import EXCEPCIONES.clsPropiedadNoExistente;

/**
 * Clase que contiene los metodos que corresponden a la gestion de articulos
 * @author Manel D.
 * @author Juan Mari D.
 * @author Jon T.
 * @author Ruben A.
 */
public abstract class clsArticulo implements itfProperty, 
Comparable<clsArticulo>
{
    
    /**
	 * Atributo del titulo del articulo
	 */
    private String str_titulo;

    /**
     * Atributo del genero del articulo
     */
    private enTipoGenero en_genero;

    /**
	 * Atributo del precio del articulo
	 */
    private int int_precio;

    /**
	 * Atributo del codigo del articulo
	 */
    private String str_codigo;

    /**
	 * Constructor para inicializar cada atributo
	 */
    public clsArticulo() 
    {
        this.str_titulo = "";
        this.en_genero = enTipoGenero.en_historico;
        this.int_precio = 0;
        this.str_codigo = "";

    }

    /**
	 * Metodo que gestiona los atributos de la clase {@link clsArticulo}
	 * @param str_paramTitulo Parametro que guarda el titulo del articulo
     * @param en_paramGenero  Parametro que guarda el genero del articulo
	 * @param int_paramPrecio Parametro que guarda el precio del articulo
	 * @param str_paramCodigo Parametro que guarda el codigo del articulo
	 */
    public clsArticulo( String str_paramTitulo, enTipoGenero en_paramGenero,
                        int int_paramPrecio, String str_paramCodigo) 
    {
        super();
        this.str_titulo = str_paramTitulo;
        this.en_genero = en_paramGenero;
        this.int_precio = int_paramPrecio;
        this.str_codigo = str_paramCodigo;

    }

    /**
	 * Metodo que recoge el titulo de cada articulo
	 * @return Retorna el titulo del articulo
	 */
    public String getTitulo() 
    {
        return this.str_titulo;
    }

    /**
	 * Metodo que recoge el genero de cada articulo
	 * @return Retorna el genero del articulo
	 */
    public enTipoGenero getGenero() 
    {
        return this.en_genero;
    }

    /**
	 * Metodo que recoge el precio de cada articulo
	 * @return Retorna el precio del articulo
	 */
    public int getPrecio() 
    {
        return this.int_precio;
    }

    /**
	 * Metodo que recoge el codigo de cada articulo
	 * @return Retorna el codigo del articulo
	 */
    public String getCodigo() 
    {
        return this.str_codigo;
    }

    /**
	 * Metodo que establece el titulo de cada articulo
	 * @param str_paramTitulo Parametro que guarda el titulo del articulo
	 */
    public void setTitulo(String str_paramTitulo) 
    {
        this.str_titulo = str_paramTitulo;
    }

    /**
	 * Metodo que establece el genero de cada articulo
	 * @param en_paramGenero Parametro que guarda el genero del articulo
	 */
    public void setGenero(enTipoGenero en_paramGenero) 
    {
        this.en_genero = en_paramGenero;
    }

    /**
	 * Metodo que establece el precio de cada articulo
	 * @param int_paramPrecio Parametro que guarda el precio del articulo
	 */
    public void setPrecio(int int_paramPrecio) 
    {
        this.int_precio = int_paramPrecio;
    }

    /**
	 * Metodo que establece el codigo de cada articulo
	 * @param str_paramCodigo Parametro que guarda el codigo del articulo
	 */
    public void setCodigo(String str_paramCodigo) 
    {
        this.str_codigo = str_paramCodigo;
    }

    @Override
    /**
	 * Metodo toString, que convierte los atributos en una cadena para
     * poder mostrarlos en pantalla
	 * @return retorna los atributos del la clase {@link clsArticulo}
	 */
    public String toString() 
    {
        return  "Titulo: " + str_titulo + "Genero: " + en_genero + 
                "Precio: " + int_precio + "Codigo: " + str_codigo ;
    }

    /**
     * Metodo equals de la clase
     */
    public boolean equals(Object obj)
    {
        if (obj instanceof clsArticulo)
        {
            clsArticulo param_articulo = (clsArticulo)obj;
            
            if (param_articulo.str_codigo.equalsIgnoreCase( str_codigo)) 
            {
                return true;   
            }   
            else
            {
                return false;
            }
        } 
        else
        {
            return false;
        }
    }

    @Override
    /**
	* Metodo que recoge las propiedades gestionadas por la clase itfProperty.
	* @param str_paramPropiedad Parametro que guarda la propiedad.
	*/
    public Object getObjectProperty(String str_paramPropiedad) 
    {
        switch ( str_paramPropiedad ) 
        {
            case clsConstantes.TITULO: return str_titulo;
            case clsConstantes.GENERO: return en_genero;
            case clsConstantes.PRECIO: return int_precio;
            case clsConstantes.CODIGO: return str_codigo;  
            default:
                throw new clsPropiedadNoExistente(str_paramPropiedad);
        }
    }

    @Override
    /**
     * Metodo para poder comparar los articulos en base al codigo para hacer
     * la ordenacion natural
     */
    public int compareTo( clsArticulo obj_unArticulo )
    {
        //el primero es menor <0 
        //el primero es mayor >0 
        //el primero es igual ==0 
        return this.str_codigo.compareTo(obj_unArticulo.str_codigo);
    }
}
