package LN;

import COMUN.clsConstantes;

/**
 * Clase que contiene los metodos que corresponden a la gestion de libros
 * @author Manel D.
 * @author Juan Mari D.
 * @author Jon T.
 * @author Ruben A.
 */
public final class clsLibro extends clsArticulo 
{
    /**
	 * Atributo de la editorial del libro
	 */
    private String str_editorial;

    /**
     * Constructor para inicializar el atributo
     */
    public clsLibro() 
    {
        super();
        this.str_editorial = "";
    }

    /**
     * Metodo que gestiona los atributos de la clase {@link clsLibro}
     * @param str_paramTitulo Parametro que guarda el titulo del libro
     * @param en_paramGenero  Parametro que guarda el genero del libro
     * @param int_paramPrecio Parametro que guarda el precio del libro
     * @param str_paramCodigo Parametro que guarda el codigo del libro
     * @param str_paramEditorial Parametro que guarda la editorial del libro
     */
    public clsLibro(String str_paramTitulo, enTipoGenero en_paramGenero, 
                    int int_paramPrecio, String str_paramCodigo,
                    String str_paramEditorial) 
    {
        super(  str_paramTitulo, en_paramGenero, int_paramPrecio,
                str_paramCodigo);
        this.str_editorial = str_paramEditorial;
    }

    /**
     * Metodo que recoge la editorial de cada libro
     * @return Retorna la editorial del libro
     */
    public String getEditorial() 
    {
        return this.str_editorial;
    }

    /**
     * Metodo que establece la editorial de cada libro
     * @param str_paramEditorial Parametro que guarda la editorial del libro
     */
    public void setEditorial(String str_paramEditorial) 
    {
        this.str_editorial = str_paramEditorial;
    }

    @Override
    /**
     * Metodo toString, que convierte los atributos en una cadena para
     * poder mostrarlos en pantalla
     * @return retorna los atributos de la clase {@link clsLibro}
     */
    public String toString() 
    {
        String str_articulo;
        str_articulo = super.toString();
        return str_articulo + " Editorial: " + str_editorial;
    }

    @Override
    /**
	* Metodo que recoge las propiedades gestionadas por la clase itfProperty.
	* @param str_paramPropiedad Parametro que guarda la propiedad.
	*/
    public Object getObjectProperty(String str_paramPropiedad) 
    {
        switch ( str_paramPropiedad ) 
        {
            case clsConstantes.EDITORIAL: return str_editorial;
            default : return super.getObjectProperty(str_paramPropiedad);
        }
    }
}