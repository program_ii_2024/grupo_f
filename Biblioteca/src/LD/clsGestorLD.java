package LD;

import java.sql.*;

import COMUN.clsConstantesLD;
import LN.enTipoGenero;

/**
 * Clase que gestiona todas las clases del paquete LD
 * @author Manel D.
 * @author Juan Mari D.
 * @author Jon T.
 * @author Ruben A.
 */
public class clsGestorLD 
{
	/**
	 * URL para la conexion
	 */
	private static final String URL = "jdbc:mysql://localhost:3306/";

	/**
	 * Nombre de la base de datos
	 */
	private static final String SCHEMA = "biblioteca";

	/**
	 * Texto necesario para la conexion
	 */
	private static final String PARAMS = 
	"?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";

	/**
	 * El driver
	 */
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";

	/**
	 * Usuario del Workbench
	 */
	private static final String USER = "root";

	/**
	 * Contrasena del Workbench
	 */
	private static final String PASS = "root";

	/**
	 * Objeto para crear la conexión a base de datos.
	 */
	Connection conexion = null;

	/**
	 * Objeto para crear la consulta a base de datos.
	 */
	PreparedStatement ps = null;

	/**
	 * Objeto para devolver el resultado de la consulta.
	 */
	ResultSet rs = null;

	/**
	 * Constructor sin parametros de la clase.
	 */
	public clsGestorLD() 
	{
	}

	/**
	 * Método para la conexion a la base de datos.
	 */
	public void conectar() 
	{
		try 
		{
			//cargamos el driver de acceso a datos
			Class.forName(DRIVER);
			conexion = DriverManager.getConnection(URL + SCHEMA + PARAMS, 
													USER, PASS);
			System.out.println("DEBUG: ¡Se ha conectado a la BD!");
		} 
		catch (Exception e) 
		{
			System.out.println("DEBUG: ERROR. No se ha conectado a la BD.");
		}
	}

	/**
	 * Metodo para la desconexion de la base de datos.
	 */
	public void desconectar()
	{
		try 
		{
			conexion.close();
			ps.close();
		} 
		catch (SQLException e) 
		{
			//nada
		} 
		finally 
		{
			try 
			{
				conexion.close();
			} 
			catch (Exception e) 
			{
				//nada
			}
			try
			{
				ps.close();
			} 
			catch (Exception e) 
			{
				//nada
			}
		}
	}

	/**
	 * Metodo para recuperar la tabla de musica de la base de datos.
	 * @return El objeto con el resultado de la consulta
	 */
	public ResultSet rs_recuperarMusica()
	{
		try 
		{
			ps = conexion.prepareStatement( clsConstantesLD.SQL_SELECT_MUSICA);
			rs = ps.executeQuery();
		} 
		catch(SQLException e)
		{
			e.printStackTrace();
		} 
		finally 
		{
			//nada
		}
		return rs;
	}

	/**
	 * Metodo para insertar musica a la base de datos.
	 * @param str_paramTitulo Titulo de la musica.
	 * @param en_paramGenero Genero de la musica.
	 * @param int_paramPrecio Precio de la musica.
	 * @param str_paramCodigo Codigo de la musica.
	 * @param int_paramAnyo Anyo de la musica.
	 */
    public void vo_insertarMusica(	String str_paramTitulo, 
									enTipoGenero en_paramGenero, 
                                    int int_paramPrecio, 
									String str_paramCodigo,
                                    int int_paramAnyo) 
    {
      	conectar();
		try 
		{
			ps = conexion.prepareStatement(clsConstantesLD.SQL_INSERT_MUSICA);
			ps.setString(1, str_paramTitulo);
			ps.setInt(2, en_paramGenero.ordinal());
			ps.setInt(3, int_paramPrecio);
			ps.setString(4, str_paramCodigo);
			ps.setInt(5, int_paramAnyo);

			ps.executeUpdate();

		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		} 
		finally 
		{
			desconectar();
		}
	}

	/**
	 * Metodo para borrar musica de la base de datos
	 * @param str_paramCodigo Codigo de la musica
	 */
	public void vo_borrarMusica(String str_paramCodigo)
	{
		conectar();

		try 
		{
			ps = conexion.prepareStatement(clsConstantesLD.SQL_DELETE_MUSICA);

			ps.setString(1, str_paramCodigo);

			ps.executeUpdate();
		} 
		catch(SQLException e) 
		{
			e.printStackTrace();
		} 
		finally 
		{
			desconectar();
		}
	}

	/**
	 * Metodo para modificar la musica en la base de datos
	 * @param str_paramTitulo Titulo de la musica.
	 * @param en_paramGenero Genero de la musica.
	 * @param int_paramPrecio Precio de la musica.
	 * @param str_paramCodigo Codigo de la musica.
	 * @param int_paramAnyo Anyo de la musica.
	 */
	public void vo_modificarMusica(	String str_paramTitulo, 
									enTipoGenero en_paramGenero, 
									int int_paramPrecio, 
									String str_paramCodigo,
									int int_paramAnyo)
	{
		conectar();
		try
		{
			ps = conexion.prepareStatement(clsConstantesLD.SQL_UPDATE_MUSICA);

			ps.setString(1, str_paramTitulo);
			ps.setInt(2, en_paramGenero.ordinal());
			ps.setInt(3, int_paramPrecio);
			ps.setString(4, str_paramCodigo);
			ps.setInt(5, int_paramAnyo);
			ps.setString(6, str_paramCodigo);

			ps.executeUpdate();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			desconectar();
		}
	}
}
