package COMUN;

import EXCEPCIONES.clsPropiedadNoExistente;

/**
 * Interfaz que intercambia datos entre la LN y LP para evitar el acoplamiento 
 * @author Manel D.
 * @author Juan Mari D.
 * @author Jon T.
 * @author Ruben A.
 */
public interface itfProperty 
{

    /**
     * Metodo get
     * @param str_paramPropiedad La propiedad
     * @return Nada
     * @throws clsPropiedadNoExistente La excepcion
     */
    public Object getObjectProperty( String str_paramPropiedad ) 
                                    throws clsPropiedadNoExistente;
   
}
