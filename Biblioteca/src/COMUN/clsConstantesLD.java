package COMUN;

/**
 * Clase que gestiona las query-s a la base de datos.
 * @author Manel D.
 * @author Juan Mari D.
 * @author Jon T.
 * @author Ruben A.
 */
public class clsConstantesLD
{

    /**
     * Query para insertar musica
     */
    public static final String SQL_INSERT_MUSICA = 
    "INSERT INTO musica (titulo, genero, precio, codigo, " + 
    "anyo) VALUES (?,?,?,?,?);";
    
    /**
     * Query para coger toda la tabla de musica
     */
    public static final String SQL_SELECT_MUSICA =
    "SELECT * FROM musica";

    /**
     * Query para modificar musica
     */
    public static final String SQL_UPDATE_MUSICA =
    "UPDATE musica SET titulo =  ?, genero = ?, precio = ?," + 
    "codigo = ?,anyo = ? WHERE (codigo = ?);";
    
    /**
     * Query para borrar musica
     */
    public static final String SQL_DELETE_MUSICA =
    "DELETE FROM musica where (codigo = ?);";
}
