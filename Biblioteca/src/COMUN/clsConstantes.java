package COMUN;

/**
 * Clase que gestiona las constantes
 * @author Manel D.
 * @author Juan Mari D.
 * @author Jon T.
 * @author Ruben A.
 */
public class clsConstantes 
{

	/**
	 * Tipos de roles del programa
	 */
    public static enum en_Rol 
	{
		/**
		 * Rol admin
		 */
		en_rolAdmin, 

		/**
		 * Rol con permisos de lectura
		 */
		en_rolRead,

		/**
		 * Rol con permisos de escritura
		 */
		en_rolWrite, 

		/**
		 * Rol invalido
		 */
		en_rolInvalid
	}

	/**
	 * Genero del articulo
	 */
	public static final String GENERO = "Genero del articulo";

	/**
	 * Titulo del articulo
	 */
	public static final String TITULO = "Titulo del articulo";
	
	/**
	 * Precio del articulo
	 */
	public static final String PRECIO = "Precio del articulo";

	/**
	 * Codigo del articulo
	 */
	public static final String CODIGO = "Codigo del articulo";

	/**
	 * Editorial del libro
	 */
	public static final String EDITORIAL = "Editorial del libro";

	/**
	 * Director de la pelicula
	 */
	public static final String DIRECTOR = "Director de la pelicula";

	/**
	 * Anyo de la musica
	 */
	 public static final String ANYO = "Anyo de la musica";

	/**
	 * Nombre del usuario
	 */
	public static final String NOMBRE = "Nombre del usuario";

	/**
	 * Email del usuario
	 */
	public static final String EMAIL  = "Email del usuario";

	/**
	 * Rol del usuario
	 */
	public static final String ROL    = "Rol del usuario";

	/**
	 * Id del usuario
	 */
	public static final String ID    = "Id del usuario";

	/**
	 * Fecha de la visita del visitante a la biblioteca
	 */
	public static final String FECHA_VISITA = 
					"Fecha de la visita del visitante a la biblioteca";

	/**
	 * Telf. del registrado
	 */
	public static final String TELEFONO_REGISTRADO = "Telf. del registrado";

	/**
	 * Area de responsabilidad del bibliotecario
	 */
	public static final String AREA_RESPONSABILIDAD = 
								"Area de responsabilidad del bibliotecario";
}




