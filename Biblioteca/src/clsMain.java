import LN.clsHilo;
import LP.clsMenu;
import LP.frmVentanaInicio;

/**
 * Clase Main del proyecto
 * 
 * @author Manel D.
 * @author Juan Mari D.
 * @author Jon T.
 * @author Ruben A.
 */
public class clsMain 
{

    /**
     * Metodo Main de la clase
     * 
     * @param args Argumentos de la funcion Main
     */
    public static void main(String[] args) 
    {
        clsHilo hilo = new clsHilo();
        hilo.start();

        clsMenu obj_m;

        new frmVentanaInicio();
        
        obj_m = new clsMenu();

        obj_m.vo_mostrarMenuinicial();
    }
}
