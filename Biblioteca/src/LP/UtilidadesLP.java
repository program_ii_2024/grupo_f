// Source code is decompiled from a .class file using FernFlower decompiler.
package LP;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * para que no salte ningun warning
 */
public class UtilidadesLP {
   
   /**
    * para que no salte ningun warning
    */
   public UtilidadesLP() {
   }

   /**
    * para que no salte ningun warning
    * @return para que no salte ningun warning
    */
   public static int leerEntero() {
      InputStreamReader isr = new InputStreamReader(System.in);
      BufferedReader br = new BufferedReader(isr);
      Integer entero = null;
      boolean error = true;

      do {
         try {
            String cadena = br.readLine();
            entero = new Integer(cadena);
            error = false;
         } catch (NumberFormatException var5) {
            System.out.println("No tecle\ufffd un n\ufffdmero entero-Repetir");
         } catch (Exception var6) {
            System.out.println("Error de entrada-Repetir ");
         }
      } while(error);

      return entero;
   }
   
   /**
    * para que no salte ningun warning
    * @return para que no salte ningun warning
    */
   public static double leerReal() {
      InputStreamReader isr = new InputStreamReader(System.in);
      BufferedReader br = new BufferedReader(isr);
      Double real = null;
      boolean error = true;

      do {
         try {
            String cadena = br.readLine();
            real = new Double(cadena);
            error = false;
         } catch (NumberFormatException var5) {
            System.out.println("No tecle\ufffd un n\ufffdmero real-Repetir ");
         } catch (Exception var6) {
            System.out.println("Error de entrada-Repetir ");
         }
      } while(error);

      return real;
   }
   
   /**
    * para que no salte ningun warning
    * @return para que no salte ningun warning
    */
   public static float leerFloat() {
      InputStreamReader isr = new InputStreamReader(System.in);
      BufferedReader br = new BufferedReader(isr);
      Float real = null;
      boolean error = true;

      do {
         try {
            String cadena = br.readLine();
            real = Float.valueOf(cadena);
            error = false;
         } catch (NumberFormatException var5) {
            System.out.println("No tecle\ufffd un n\ufffdmero real-Repetir ");
         } catch (Exception var6) {
            System.out.println("Error de entrada-Repetir ");
         }
      } while(error);

      return real;
   }

   /**
    * para que no salte ningun warning
    * @return para que no salte ningun warning
    */
   public static char leerCaracter() {
      char caracter = 0;
      boolean error = true;

      do {
         try {
            caracter = (char)System.in.read();
            System.in.skip((long)System.in.available());
            error = false;
         } catch (Exception var3) {
            System.out.println("Error de entrada-Repetir ");
         }
      } while(error);

      return caracter;
   }

   /**
    * para que no salte ningun warning
    * @return para que no salte ningun warning
    */
   public static String leerCadena() {
      InputStreamReader isr = new InputStreamReader(System.in);
      BufferedReader br = new BufferedReader(isr);
      String cadena = null;
      boolean error = true;

      do {
         try {
            cadena = br.readLine();
            error = false;
         } catch (Exception var5) {
            System.out.println("Error de entrada-Repetir ");
         }
      } while(error);

      return cadena;
   }
}
