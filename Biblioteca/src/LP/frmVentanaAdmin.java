package LP;

import javax.swing.*;

import COMUN.itfProperty;
import EXCEPCIONES.clsArticuloRepetido;
import LN.clsGestorLN;
import LN.enTipoGenero;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class frmVentanaAdmin extends JFrame implements ActionListener {

    private JTabbedPane tabbedPane;
    private JTextField textField;
    private JTextField textField_2;
    private JTextField textField_3;
    private JTextField textField_4;
    private JTextField textField_5;
    private JTextField textField_6;
    private JTextField textField_7;
    private JTextField textField_1;
    private JTextField textField_8;
    private JList<itfProperty> list;
    private JRadioButton rdbtnNewRadioButton_1;
    private JRadioButton rdbtnNewRadioButton_1_1;
    private JRadioButton rdbtnNewRadioButton_1_2;
    private JRadioButton rdbtnNewRadioButton_1_3;
    private final ButtonGroup buttonGroup = new ButtonGroup();
    private final ButtonGroup buttonGroup_1 = new ButtonGroup();
    ArrayList<itfProperty> lista;
    private DefaultListModel<itfProperty> dlm;

    private clsGestorLN gln;

    public frmVentanaAdmin(clsGestorLN gln) {
        this.gln = gln;
        // Configuración del JFrame
        setTitle("                      Ventana Administrador");
        setSize(400, 300);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Crear el JTabbedPane
        tabbedPane = new JTabbedPane();

        JPanel panel3 = new JPanel();
        tabbedPane.addTab("Consultar", panel3);
        panel3.setLayout(null);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(30, 25, 148, 189);
        panel3.add(scrollPane);

        list = new JList<>();
        scrollPane.setViewportView(list);

        JButton btnNewButton_2 = new JButton("Orden Natural");
        btnNewButton_2.setBounds(188, 77, 142, 21);
        panel3.add(btnNewButton_2);

        JButton btnNewButton_2_1 = new JButton("Orden Especifico");
        btnNewButton_2_1.setBounds(188, 121, 142, 21);
        panel3.add(btnNewButton_2_1);

        JPanel panel2 = new JPanel();
        panel2.setLayout(null);
        JLabel label = new JLabel("");
        label.setBounds(241, 14, 0, 0);
        panel2.add(label);
        tabbedPane.addTab("Modificar", panel2);

        textField_1 = new JTextField();
        textField_1.setColumns(10);
        textField_1.setBounds(10, 56, 96, 19);
        panel2.add(textField_1);

        JLabel lblNewLabel_1 = new JLabel("Codigo de busqueda ");
        lblNewLabel_1.setBounds(10, 39, 132, 13);
        panel2.add(lblNewLabel_1);

        JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Titulo");
        buttonGroup_1.add(rdbtnNewRadioButton_1);
        rdbtnNewRadioButton_1.setBounds(10, 81, 103, 21);
        panel2.add(rdbtnNewRadioButton_1);

        JRadioButton rdbtnNewRadioButton_1_1 = new JRadioButton("Genero");
        buttonGroup_1.add(rdbtnNewRadioButton_1_1);
        rdbtnNewRadioButton_1_1.setBounds(10, 104, 103, 21);
        panel2.add(rdbtnNewRadioButton_1_1);

        JRadioButton rdbtnNewRadioButton_1_2 = new JRadioButton("Precio");
        buttonGroup_1.add(rdbtnNewRadioButton_1_2);
        rdbtnNewRadioButton_1_2.setBounds(10, 127, 103, 21);
        panel2.add(rdbtnNewRadioButton_1_2);

        JRadioButton rdbtnNewRadioButton_1_3 = new JRadioButton("Anyo");
        buttonGroup_1.add(rdbtnNewRadioButton_1_3);
        rdbtnNewRadioButton_1_3.setBounds(10, 150, 103, 21);
        panel2.add(rdbtnNewRadioButton_1_3);

        textField_8 = new JTextField();
        textField_8.setBounds(158, 105, 96, 19);
        panel2.add(textField_8);
        textField_8.setColumns(10);

        JLabel lblNewLabel_1_1 = new JLabel("Nuevo dato");
        lblNewLabel_1_1.setBounds(158, 85, 96, 13);
        panel2.add(lblNewLabel_1_1);

        JButton btnNewButton_1 = new JButton("Modificar");
        btnNewButton_1.setBounds(158, 163, 96, 21);
        panel2.add(btnNewButton_1);

        // Crear paneles para cada pestaña
        JPanel panel1 = new JPanel();
        panel1.setLayout(null);

        textField = new JTextField();
        textField.setBounds(139, 80, 96, 19);
        panel1.add(textField);
        textField.setColumns(10);

        // Añadir los paneles al JTabbedPane
        tabbedPane.addTab("Add / Delete", panel1);

        JLabel lblNewLabel = new JLabel("Codigo de busqueda ");
        lblNewLabel.setBounds(139, 63, 131, 13);
        panel1.add(lblNewLabel);

        JButton btnNewButton = new JButton("Add ");
        btnNewButton.setBounds(269, 161, 85, 21);
        panel1.add(btnNewButton);

        JButton btnDelete = new JButton("Delete");
        btnDelete.setBounds(269, 122, 85, 21);
        panel1.add(btnDelete);

        JRadioButton rdbtnNewRadioButton = new JRadioButton("Libro");
        buttonGroup.add(rdbtnNewRadioButton);
        rdbtnNewRadioButton.setBounds(19, 16, 103, 21);
        panel1.add(rdbtnNewRadioButton);

        JRadioButton rdbtnPelicula = new JRadioButton("Pelicula");
        buttonGroup.add(rdbtnPelicula);
        rdbtnPelicula.setBounds(19, 39, 103, 21);
        panel1.add(rdbtnPelicula);

        JRadioButton rdbtnMusica = new JRadioButton("Musica");
        buttonGroup.add(rdbtnMusica);
        rdbtnMusica.setBounds(19, 62, 103, 21);
        panel1.add(rdbtnMusica);

        textField_2 = new JTextField();
        textField_2.setColumns(10);
        textField_2.setBounds(19, 123, 96, 19);
        panel1.add(textField_2);

        textField_3 = new JTextField();
        textField_3.setColumns(10);
        textField_3.setBounds(19, 167, 96, 19);
        panel1.add(textField_3);

        textField_4 = new JTextField();
        textField_4.setColumns(10);
        textField_4.setBounds(19, 206, 96, 19);
        panel1.add(textField_4);

        textField_5 = new JTextField();
        textField_5.setColumns(10);
        textField_5.setBounds(139, 206, 96, 19);
        panel1.add(textField_5);

        textField_6 = new JTextField();
        textField_6.setColumns(10);
        textField_6.setBounds(139, 163, 96, 19);
        panel1.add(textField_6);

        textField_7 = new JTextField();
        textField_7.setColumns(10);
        textField_7.setBounds(139, 120, 96, 19);
        panel1.add(textField_7);

        JLabel lblPrecio_1 = new JLabel("Director");
        lblPrecio_1.setBounds(19, 190, 96, 13);
        panel1.add(lblPrecio_1);

        JLabel lblPrecio = new JLabel("Anyo");
        lblPrecio.setBounds(19, 149, 96, 13);
        panel1.add(lblPrecio);

        JLabel lblGenero = new JLabel("Editorial");
        lblGenero.setBounds(19, 104, 96, 13);
        panel1.add(lblGenero);

        JLabel lblTitulo = new JLabel("Titulo");
        lblTitulo.setBounds(139, 105, 96, 13);
        panel1.add(lblTitulo);

        JLabel lblGenero_1 = new JLabel("Genero");
        lblGenero_1.setBounds(139, 149, 96, 13);
        panel1.add(lblGenero_1);

        JLabel lblPrecio_2 = new JLabel("Precio");
        lblPrecio_2.setBounds(139, 192, 96, 13);
        panel1.add(lblPrecio_2);

        // Añadir el JTabbedPane al JFrame
        getContentPane().add(tabbedPane, BorderLayout.CENTER);
        setVisible(true);

        dlm = new DefaultListModel<>();
        list.setModel(dlm);

        lista = gln.obj_recuperarArticulos();
        dlm.addAll(lista);

        btnNewButton_1.setActionCommand("Modificar");
        btnNewButton_1.addActionListener(this);
        rdbtnNewRadioButton_1.setActionCommand("Titulo");
        rdbtnNewRadioButton_1.addActionListener(this);
        rdbtnNewRadioButton_1_1.setActionCommand("Genero");
        rdbtnNewRadioButton_1_1.addActionListener(this);
        rdbtnNewRadioButton_1_2.setActionCommand("Precio");
        rdbtnNewRadioButton_1_2.addActionListener(this);
        rdbtnNewRadioButton_1_3.setActionCommand("Anyo");
        rdbtnNewRadioButton_1_3.addActionListener(this);

        btnNewButton_2.setActionCommand("Orden Natural");
        btnNewButton_2.addActionListener(this);
        btnDelete.setActionCommand("Delete");
        btnDelete.addActionListener(this);
        btnNewButton.setActionCommand("Add");
        btnNewButton.addActionListener(this);

    }

    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Modificar":
                if (rdbtnNewRadioButton_1.isSelected() == true) {
                    boolean bln_res = gln.bln_ModificarTituloMusica(textField_1.getText(), textField_8.getText());

                    if (bln_res) {
                        System.out.println("DEBUG: Se ha modificado correctamente");
                    } else {
                        System.out.println("Debog: No se ha realizado correctamente");
                    }
                    break;
                } else if (rdbtnNewRadioButton_1_1.isSelected() == true) {
                    enTipoGenero genero = enTipoGenero.valueOf(textField_8.getText());
                    boolean bln_res = gln.bln_ModificarGeneroMusica(textField_1.getText(), genero);

                    if (bln_res) {
                        System.out.println("DEBUG: Se ha modificado correctamente");
                    } else {
                        System.out.println("Debog: No se ha realizado correctamente");
                    }
                    break;
                } else if (rdbtnNewRadioButton_1_2.isSelected() == true) {
                    boolean bln_res = gln.bln_ModificarPrecioMusica(textField_1.getText(),
                            Integer.parseInt(textField_8.getText()));

                    if (bln_res) {
                        System.out.println("DEBUG: Se ha modificado correctamente");
                    } else {
                        System.out.println("Debog: No se ha realizado correctamente");
                    }
                    break;
                } else if (rdbtnNewRadioButton_1_3.isSelected() == true) {
                    boolean bln_res = gln.bln_ModificarAnyoMusica(textField_1.getText(),
                            Integer.parseInt(textField_8.getText()));

                    if (bln_res) {
                        System.out.println("DEBUG: Se ha modificado correctamente");
                    } else {
                        System.out.println("Debog: No se ha realizado correctamente");
                    }
                    break;
                }

            case "Orden Natural":

                dlm.clear();
                List<itfProperty> r = gln.obj_recuperarArticulos();

                dlm.addAll(r);
                break;

            case "Delete":

                if (gln.bln_borrarMusica(textField.getText())) {
                    System.out.println("DEBUG: Se ha borrado");
                } else {
                    System.out.println("DEBUG: No se ha borrado");
                }
                break;

            case "Add":
                enTipoGenero en = null;

                if (textField_2.getText() == "" && textField_4.getText() == "") {
                    // Musica
                    switch (textField_6.getText()) {
                        case "Historico":
                            try {
                                en = enTipoGenero.en_historico;
                                gln.vo_InsertarMusica(textField_7.getText(), en,
                                        Integer.parseInt(textField_5.getText()),
                                        textField.getText(), Integer.parseInt(textField_3.getText()));
                                System.out.println("Musica insertada correctamente");
                            } catch (clsArticuloRepetido o) {
                                System.out.println(o.mensajeError());
                            }

                            break;
                        case "Terror":

                            try {
                                en = enTipoGenero.en_terror;
                                gln.vo_InsertarMusica(textField_7.getText(), en,
                                        Integer.parseInt(textField_5.getText()),
                                        textField.getText(), Integer.parseInt(textField_3.getText()));
                                System.out.println("Musica insertada correctamente");
                            } catch (clsArticuloRepetido o) {
                                System.out.println(o.mensajeError());
                            }

                            break;
                        case "Accion":
                            try {
                                en = enTipoGenero.en_accion;
                                gln.vo_InsertarMusica(textField_7.getText(), en,
                                        Integer.parseInt(textField_5.getText()),
                                        textField.getText(), Integer.parseInt(textField_3.getText()));
                                System.out.println("Musica insertada correctamente");
                            } catch (clsArticuloRepetido o) {
                                System.out.println(o.mensajeError());
                            }
                            break;
                        case "Romantico":
                            try {
                                en = enTipoGenero.en_romantico;
                                gln.vo_InsertarMusica(textField_7.getText(), en,
                                        Integer.parseInt(textField_5.getText()),
                                        textField.getText(), Integer.parseInt(textField_3.getText()));
                                System.out.println("Musica insertada correctamente");
                            } catch (clsArticuloRepetido o) {
                                System.out.println(o.mensajeError());
                            }
                            break;
                        default:
                            System.out.println("DEBUG: No se han insertado los datos correctamente");
                            break;
                    }
                } else if (textField_3.getText() == "" && textField_4.getText() == "") {
                    // Libros
                    switch (textField_6.getText()) {
                        case "Historico":

                            try {
                                en = enTipoGenero.en_historico;
                                gln.vo_InsertarLibro(textField_7.getText(), en, Integer.parseInt(textField_5.getText()),
                                        textField.getText(), textField_2.getText());
                                System.out.println("Pelicula insertada correctamente");
                            } catch (clsArticuloRepetido o) {
                                System.out.println(o.mensajeError());
                            }

                            break;
                        case "Terror":
                            try {
                                en = enTipoGenero.en_terror;
                                gln.vo_InsertarLibro(textField_7.getText(), en, Integer.parseInt(textField_5.getText()),
                                        textField.getText(), textField_2.getText());
                                System.out.println("Pelicula insertada correctamente");
                            } catch (clsArticuloRepetido o) {
                                System.out.println(o.mensajeError());
                            }
                            break;
                        case "Accion":
                            try {
                                en = enTipoGenero.en_accion;
                                gln.vo_InsertarLibro(textField_7.getText(), en, Integer.parseInt(textField_5.getText()),
                                        textField.getText(), textField_2.getText());
                                System.out.println("Pelicula insertada correctamente");
                            } catch (clsArticuloRepetido o) {
                                System.out.println(o.mensajeError());
                            }
                            break;
                        case "Romantico":
                            try {
                                en = enTipoGenero.en_romantico;
                                gln.vo_InsertarLibro(textField_7.getText(), en, Integer.parseInt(textField_5.getText()),
                                        textField.getText(), textField_2.getText());
                                System.out.println("Pelicula insertada correctamente");
                            } catch (clsArticuloRepetido o) {
                                System.out.println(o.mensajeError());
                            }
                        default:
                            System.out.println("DEBUG: No se han insertado los datos correctamente");
                            break;

                    }
                } else if (textField_2.getText() == "" && textField_3.getText() == "") {

                    // Pelicula
                    switch (textField_6.getText()) {
                        case "Historico":

                            try {
                                en = enTipoGenero.en_historico;
                                gln.vo_InsertarPelicula(textField_7.getText(), en,
                                        Integer.parseInt(textField_5.getText()),
                                        textField.getText(), textField_4.getText());
                                System.out.println("Pelicula insertada correctamente");
                            } catch (clsArticuloRepetido o) {
                                System.out.println(o.mensajeError());
                            }

                            break;
                        case "Terror":
                            try {
                                en = enTipoGenero.en_terror;
                                gln.vo_InsertarLibro(textField_7.getText(), en, Integer.parseInt(textField_5.getText()),
                                        textField.getText(), textField_4.getText());
                                System.out.println("Pelicula insertada correctamente");
                            } catch (clsArticuloRepetido o) {
                                System.out.println(o.mensajeError());
                            }
                            break;
                        case "Accion":
                            try {
                                en = enTipoGenero.en_accion;
                                gln.vo_InsertarLibro(textField_7.getText(), en, Integer.parseInt(textField_5.getText()),
                                        textField.getText(), textField_4.getText());
                                System.out.println("Pelicula insertada correctamente");
                            } catch (clsArticuloRepetido o) {
                                System.out.println(o.mensajeError());
                            }
                            break;
                        case "Romantico":
                            try {
                                en = enTipoGenero.en_romantico;
                                gln.vo_InsertarLibro(textField_7.getText(), en, Integer.parseInt(textField_5.getText()),
                                        textField.getText(), textField_4.getText());
                                System.out.println("Pelicula insertada correctamente");
                            } catch (clsArticuloRepetido o) {
                                System.out.println(o.mensajeError());
                            }
                        default:
                            System.out.println("DEBUG: No se han insertado los datos correctamente");
                            break;
                    }
                } else {
                    System.out.println("DEBUG: No se tiene datos suficientes");
                }
                break;

            default:
                break;

        }
    }
}
