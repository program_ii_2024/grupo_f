package LP;

import java.util.ArrayList;

import COMUN.clsConstantes;
import COMUN.itfProperty;
import EXCEPCIONES.clsArticuloRepetido;
import EXCEPCIONES.clsCodigoIncorrecto;
import LN.enTipoGenero;
import LN.clsGestorLN;
import LN.clsHilo;

/**
 * Clase Menu del proyecto
 * 
 * @author Manel D.
 * @author Juan Mari D.
 * @author Jon T.
 * @author Ruben A.
 */
public class clsMenu 
{

    /**
     * Declaracion del objeto de la clase {@link clsGestorLN}
     */
    private clsGestorLN objGestorLN;

    /**
     * Metodo constructor que incializa los atributos necesarios.
     */
    public clsMenu() 
    {
        objGestorLN = new clsGestorLN();

    }

    /**
     * Metodo para mostrar el munu inicial
     */
    public void vo_mostrarMenuinicial() 
    {
        int int_opcion = -1;

        while (int_opcion != 4) 
        {
            System.out.println("¡LE DAMOS LA BIENVENIDA A LA BIBLIOTECA!");
            System.out.println("________________________________________");
            System.out.println("                                        ");
            System.out.println("      Seleccione el rol que desee:      ");
            System.out.println("1.Visitante 2.Registrado 3.Bibliotecario");
            System.out.println("         Pulse 4 para salir...          ");
            System.out.println("________________________________________");
            System.out.println("                                        ");

            int_opcion = UtilidadesLP.leerEntero();

            switch (int_opcion) 
            {
                case 1:
                    vo_mostrarMenuvisitante();
                    break;

                case 2:
                    vo_menuRegistrado();
                    break;

                case 3:
                    vo_menuBibliotecario();
                    break;

                case 4:
                    System.out.println("¡Hasta la proxima!");
                    break;

                default:
                    System.out.println("Opción no valida...");
                    break;
            }
        }
    }

    /**
     * Metodo para mostrar el menu visitante
     */
    private void vo_mostrarMenuvisitante() 
    {

        int int_opcion_v = -1;

        while (int_opcion_v != 4) 
        {
            System.out.println("         ¡BIENVENIDO VISITANTE!            ");
            System.out.println("___________________________________________");
            System.out.println("                                           ");
            System.out.println("      Seleccione la opcion que quiera:     ");
            System.out.println("        1.Ver todos los libros:            ");
            System.out.println("         2.Ver toda la musica:             ");
            System.out.println("       3.Ver todas las peliculas:          ");
            System.out.println("                                           ");
            System.out.println("           Pulse 4 para salir...           ");
            System.out.println("___________________________________________");
            System.out.println("                                           ");

            int_opcion_v = UtilidadesLP.leerEntero();

            switch (int_opcion_v) 
            {
                case 1:
                    vo_visualizarLibros();
                    break;

                case 2:
                    vo_visualizarMusica();
                    break;

                case 3:
                    vo_visualizarPelicula();
                    break;

                case 4:
                    System.out.println("Has salido");
                    break;

                default:
                    System.out.println("Opción no valida...");
                    break;
            }
        }
    }

    /*
     * metodo privado del menu, a traves del cual recorreremos el arraylist de
     * usuarios, con el fi de saber si el usuario que se esta introduciendo ya
     * esta registrado en el sistema.
     * A pesar de que el nombre no es un identificativo clave unico, en nuestro
     * metodos de insertar usuarios, hemos aplicado equals, para que no se pueda
     * introducir 2 usuarios con el mismo nombre, de esta manera la busqueda se
     * realizara de manera correcta.
     */
    private void vo_menuRegistrado() 
    {
        String str_nombre;
        int int_id;
        boolean bln_respuesta_nom;
        boolean bln_respuesta_id;

        System.out.println("Escribe el nombre del usuario registrado");
        str_nombre = UtilidadesLP.leerCadena();

        bln_respuesta_nom = objGestorLN.bln_esUsuarioRegistrado(str_nombre);

        System.out.println("Escribe el ID del usuario");
        int_id = UtilidadesLP.leerEntero();

        bln_respuesta_id = objGestorLN.bln_comprobarUsuario(str_nombre, int_id);

        if (bln_respuesta_nom && bln_respuesta_id) 
        {
            vo_mostrarMenuRegistrado(str_nombre);

        } else {
            System.out.println("Ese usuario no esta registrado");
        }
    }

    /**
     * Metodo menu bibliotecario
     */
    private void vo_menuBibliotecario() 
    {
        String str_nombre;
        int int_id;
        boolean bln_respuesta_nom;
        boolean bln_respuesta_id;

        System.out.println("Escribe el nombre del usuario Bibliotecario");
        str_nombre = UtilidadesLP.leerCadena();

        bln_respuesta_nom = objGestorLN.bln_esUsuarioBibliotecario(str_nombre);

        System.out.println("Escribe el ID del usuario");
        int_id = UtilidadesLP.leerEntero();

        bln_respuesta_id = objGestorLN.bln_comprobarUsuario(str_nombre, int_id);

        if (bln_respuesta_nom && bln_respuesta_id) 
        {
            vo_mostrarMenuBibliotecario(str_nombre);

        } else 
        {
            System.out.println("Ese Bibliotecario no esta registrado");
        }
    }

    /**
     * Metodo para mostrar el menu registrado
     * 
     * @param str_nombre Nombre del usuario
     */
    private void vo_mostrarMenuRegistrado(String str_nombre) 
    {

        int int_opcion_r = -1;

        while (int_opcion_r != 5) 
        {
            System.out.println("      ¡BIENVENIDO " + str_nombre + "!        ");
            System.out.println("___________________________________________");
            System.out.println("                                           ");
            System.out.println("      Seleccione la opcion que quiera:     ");
            System.out.println("   1.Modificar sus datos como usuario:     ");
            System.out.println("        2.Ver todos los libros:            ");
            System.out.println("         3.Ver toda la musica:             ");
            System.out.println("       4.Ver todas las peliculas:          ");
            System.out.println("                                           ");
            System.out.println("           Pulse 5 para salir...           ");
            System.out.println("___________________________________________");
            System.out.println("                                           ");

            int_opcion_r = UtilidadesLP.leerEntero();

            switch (int_opcion_r) 
            {
                case 1:
                    vo_menuModificarusuario(str_nombre);
                    int_opcion_r = 5;
                    break;

                case 2:
                    vo_visualizarLibros();
                    break;

                case 3:
                    vo_visualizarMusica();
                    break;

                case 4:
                    vo_visualizarPelicula();
                    break;

                case 5:
                    System.out.println("Has salido");
                    break;

                default:
                    System.out.println("Opción no valida...");
                    break;
            }
        }
    }

    /**
     * Metodo para modificar la informacion del usuario
     * 
     * @param str_paramNombre Nombre del usuario
     */
    private void vo_menuModificarusuario(String str_paramNombre) 
    {
        String str_newNombre;
        String str_newEmail;
        int int_newId;
        int int_opcion = -1;
        boolean bln_resEmail;
        boolean bln_resNom;
        boolean bln_resId;

        while (int_opcion != 4) 
        {
            System.out.println("Datos del usuario a modificar");
            System.out.println("    1. Email de " + str_paramNombre);
            System.out.println("    2. ID de " + str_paramNombre);
            System.out.println("    3. Nombre de " + str_paramNombre);
            System.out.println("    4. Salir ");

            int_opcion = UtilidadesLP.leerEntero();

            switch (int_opcion) 
            {
                case 1:
                    System.out.println("Introduce el nuevo Email");
                    str_newEmail = UtilidadesLP.leerCadena();

                    bln_resEmail = objGestorLN.bln_modificarEmailUsuario(
                            str_paramNombre, str_newEmail);

                    if (bln_resEmail) 
                    {
                        System.out.println("El Email se ha cambiado!");
                        int_opcion = 4;
                    } else 
                    {
                     System.out.println(" El Email no se ha podido cambiar!");
                    }
                    break;

                case 2:
                    System.out.println("Introduce el nuevo ID");
                    int_newId = UtilidadesLP.leerEntero();

                    bln_resId = 
                    objGestorLN.bln_modificarIdUsuario(str_paramNombre,
                                                       int_newId);

                    if (bln_resId) 
                    {
                        System.out.println("El Id del usuario ha cambiado!");
                        int_opcion = 4;
                    } else 
                    {
                       System.out.println("El Id no se ha podido modificar!");
                    }
                    break;

                case 3:
                    System.out.println("Introduce el nuevo Nombre");
                    str_newNombre = UtilidadesLP.leerCadena();
                    bln_resNom = objGestorLN.bln_modificarNombreUsuario(
                            str_paramNombre, str_newNombre);

                    if (bln_resNom) 
                    {
                        System.out.println("El Nombre ha sido guardado!");
                        int_opcion = 4;
                    } else 
                    {
                        System.out.println("El Nombre no se ha cambiado");
                    }
                    break;

                case 4:
                    System.out.println("Has salido");
                    break;

                default:
                    System.out.println("Opcion no valida...");
                    break;
            }
        }
    }

    /**
     * Menu de bibliotecario (rol: admin)
     */
    private void vo_mostrarMenuBibliotecario(String str_nombre) 
    {
        int int_opcion_b = -1;

        while (int_opcion_b != 4) 
        {
            System.out.println("        ¡BIENVENIDO " + str_nombre + "!      ");
            System.out.println("___________________________________________");
            System.out.println("                                           ");
            System.out.println("Seleccione el articulo que desee gestionar:");
            System.out.println("  1.Libros      2.Musica      3.Peliculas  ");
            System.out.println("           Pulse 4 para salir...           ");
            System.out.println("___________________________________________");
            System.out.println("                                           ");

            int_opcion_b = UtilidadesLP.leerEntero();

            switch (int_opcion_b) 
            {
                case 1:
                    vo_menuLibros();
                    break;

                case 2:
                    vo_menuMusica();
                    break;

                case 3:
                    vo_menuPeliculas();
                    break;

                case 4:
                    System.out.println("Has salido");
                    break;

                default:
                    System.out.println("Opción no valida...");
                    break;
            }
        }
    }

    /**
     * Menu de visualizacion de las opciones de los libros
     */
    private void vo_menuLibros() 
    {
        int int_opcion_b_l = -1;

        while (int_opcion_b_l != 5) 
        {
            System.out.println("       ESTE ES EL MENU DE LIBROS        ");
            System.out.println("________________________________________");
            System.out.println("                                        ");
            System.out.println("Seleccione la accion que desee realizar:");
            System.out.println("           1. Agregar libro             ");
            System.out.println("           2. Borrar libro              ");
            System.out.println("           3. Modificar libro           ");
            System.out.println("           4. Visualizar libros         ");
            System.out.println("           5. Salir                     ");
            System.out.println("________________________________________");
            System.out.println("                                        ");

            int_opcion_b_l = UtilidadesLP.leerEntero();

            switch (int_opcion_b_l) 
            {
                case 1:
                    vo_agregarLibro();
                    break;

                case 2:
                    vo_borrarLibro();
                    break;

                case 3:
                    vo_modificarLibro();
                    break;

                case 4:
                    vo_visualizarLibros();
                    break;

                case 5:
                    System.out.println("Has salido");
                    break;

                default:
                    System.out.println("Opción no valida...");
                    break;
            }
        }
    }

    /**
     * Menu de visualizacion de las opciones de la musica
     */
    private void vo_menuMusica() 
    {
        int int_opcion_b_m = -1;

        while (int_opcion_b_m != 5) 
        {
            System.out.println("       ESTE ES EL MENU DE MUSICA        ");
            System.out.println("________________________________________");
            System.out.println("                                        ");
            System.out.println("Seleccione la accion que desee realizar:");
            System.out.println("           1. Agregar musica            ");
            System.out.println("           2. Borrar musica             ");
            System.out.println("           3. Modificar musica          ");
            System.out.println("           4. Visualizar musica         ");
            System.out.println("           5. Salir                     ");
            System.out.println("________________________________________");
            System.out.println("                                        ");

            int_opcion_b_m = UtilidadesLP.leerEntero();

            switch (int_opcion_b_m) 
            {
                case 1:
                    vo_agregarMusica();
                    break;

                case 2:
                    vo_borrarMusica();
                    break;

                case 3:
                    vo_modificarMusica();
                    break;

                case 4:
                    vo_visualizarMusica();
                    break;

                case 5:
                    System.out.println("Has salido");
                    break;

                default:
                    System.out.println("Opción no valida...");
                    break;
            }
        }
    }

    /**
     * Funcion que nos permite modificar los elementos de tipo clsMusica
     */
    private void vo_modificarMusica()
    {
        int int_opcion = -1;
        String str_codigo;

        String str_newCodigo;
        String str_newTitulo;
        enTipoGenero en_newGenero;
        int int_newPrecio;
        int int_newAnyo;

        boolean bln_resGenero;
        boolean bln_resTitulo;
        boolean bln_resCodigo;
        boolean bln_resPrecio;
        boolean bln_resAnyo;

        vo_visualizarMusica();

        System.out.println("Codigo de la musica que desead modificar: ");
        str_codigo = UtilidadesLP.leerCadena();

        try 
        {
            objGestorLN.bln_buscarArticulo(str_codigo);
        } 
        catch (clsCodigoIncorrecto e) 
        {
            System.out.println(e.mensajeError());
            int_opcion = 6;
        }
    

        while (int_opcion != 6) 
        {
            System.out.println("Datos de la musica que desea modificar:");
            System.out.println("    1. Codigo");
            System.out.println("    2. Titulo");
            System.out.println("    3. Genero");
            System.out.println("    4. Precio");
            System.out.println("    5. Anyo");
            System.out.println("    6. Salir");

            int_opcion = UtilidadesLP.leerEntero();

            switch (int_opcion) 
            {
                case 1:
                    System.out.println("Escribe el nuevo Codigo: ");
                    str_newCodigo = UtilidadesLP.leerCadena(); 

                    bln_resCodigo = 
                    objGestorLN.bln_ModificarCodigoMusica(str_codigo, 
                                                          str_newCodigo);

                    if (bln_resCodigo) 
                    {
                        System.out.println("Se ha modificado el Codigo");    
                    }
                    else
                    {
                        System.out.println("No se ha modificado el Codigo");
                    }
                    int_opcion = 6;
                    break;

                case 2: 
                    System.out.println("Escribe el nuevo Titulo: ");
                    str_newTitulo = UtilidadesLP.leerCadena();
                    bln_resTitulo = 
                    objGestorLN.bln_ModificarTituloMusica(str_codigo, 
                                                          str_newTitulo);
                    
                    if (bln_resTitulo) 
                    {
                        System.out.println("Se ha modificado el Titulo");    
                    }
                    else
                    {
                        System.out.println("No se ha cambiado el Titulo");
                    }
                    int_opcion = 6;
                    break;

                case 3: 
                    en_newGenero = en_seleccionarTipo();
                    bln_resGenero = 
                    objGestorLN.bln_ModificarGeneroMusica(str_codigo, 
                                                          en_newGenero);
                    if (bln_resGenero) 
                    {
                        System.out.println("Se ha modificado el genero");    
                    }
                    else
                    {
                        System.out.println("No se ha modificado el genero");
                    }
                    int_opcion = 6;
                    break;

                case 4: 
                    System.out.println("Escribe el nuevo precio");
                    int_newPrecio = UtilidadesLP.leerEntero();
                    bln_resPrecio = 
                    objGestorLN.bln_ModificarPrecioMusica(str_codigo, 
                                                          int_newPrecio);
                    if (bln_resPrecio) 
                    {
                        System.out.println("El precio ha sido modificado");    
                    }
                    else
                    {
                        System.out.println("No se ha modificado el precio");
                    }
                    int_opcion = 6;
                    break;

                case 5: 
                    System.out.println("Escribe el nuevo Anyo");
                    int_newAnyo = UtilidadesLP.leerEntero();
                    bln_resAnyo = objGestorLN.bln_ModificarAnyoMusica(str_codigo, int_newAnyo);
                    if (bln_resAnyo) 
                    {
                        System.out.println("Se ha modificado el Anyo");    
                    }
                    else
                    {
                        System.out.println("No se ha modificado el Anyo");
                    }
                    int_opcion = 6;
                    break;
                    
                case 6:
                    System.out.println("Has salido");
                    int_opcion = 6;
                    break;
                default:
                    System.out.println("Introduce una opción valida");
                    break;
            }
        }
    }

    /**
     * Menu de visualizacion de las opciones de las peliculas
     */
    private void vo_menuPeliculas() 
    {
        int int_opcion_b_p = -1;

        while (int_opcion_b_p != 5) 
        {
            System.out.println("      ESTE ES EL MENU DE PELICULAS      ");
            System.out.println("________________________________________");
            System.out.println("                                        ");
            System.out.println("Seleccione la accion que desee realizar:");
            System.out.println("         1. Agregar pelicula            ");
            System.out.println("         2. Borrar pelicula             ");
            System.out.println("         3. Modificar pelicula          ");
            System.out.println("         4. Visualizar peliculas        ");
            System.out.println("         5. Salir                       ");
            System.out.println("________________________________________");
            System.out.println("                                        ");

            int_opcion_b_p = UtilidadesLP.leerEntero();

            switch (int_opcion_b_p) 
            {
                case 1:
                    vo_agregarPelicula();
                    break;

                case 2:
                    vo_borrarPelicula();
                    break;

                case 3:
                    vo_modificarPelicula();
                    break;

                case 4:
                    vo_visualizarPelicula();
                    break;

                case 5:
                    System.out.println("Has salido");
                    break;

                default:
                    System.out.println("Opción no valida...");
                    break;
            }
        }
    }

    /**
     * Metodo para agregar libros a la biblioteca
     */
    private void vo_agregarLibro() 
    {
        String str_titulo;
        enTipoGenero en_Genero;
        int int_precio;
        String str_codigo;
        String str_editorial;
        String str_opcionGenero;
        en_Genero = null;

        System.out.print("Inserta el titulo: ");
        str_titulo = UtilidadesLP.leerCadena();

        while (en_Genero == null) 
        {
            System.out.println("Elige el genero:");
            System.out.println("a: Historico");
            System.out.println("b: Terror");
            System.out.println("c: Accion");
            System.out.println("d: Romantico");
    
            str_opcionGenero = UtilidadesLP.leerCadena();
    
            switch (str_opcionGenero) 
            {
                case "a":
                    en_Genero = enTipoGenero.en_historico;
                    break;
    
                case "b":
                    en_Genero = enTipoGenero.en_terror;
                    break;
    
                case "c":
                    en_Genero = enTipoGenero.en_accion;
                    break;
    
                case "d":
                    en_Genero = enTipoGenero.en_romantico;
                    break;
    
                default:
                    System.out.println("Seleccione una opcion valida...");
            }   
        }

        System.out.print("Inserta el precio: ");
        int_precio = UtilidadesLP.leerEntero();

        System.out.print("Inserta el codigo: ");
        str_codigo = UtilidadesLP.leerCadena();

        System.out.print("Inserta la editorial: ");
        str_editorial = UtilidadesLP.leerCadena();

        try
        {
            objGestorLN.vo_InsertarLibro(
                str_titulo, en_Genero, int_precio, str_codigo, str_editorial);
            System.out.println("Libro insertado correctamente");
        }
        catch (clsArticuloRepetido e)
        {
            System.out.println(e.mensajeError());
        }
        
    }

    /**
     * Metodo para borrar libros de la biblioteca
     */
    private void vo_borrarLibro() 
    {
        String str_codigo;

        vo_visualizarLibros();

        System.out.println("Escoge el codigo del libro que desas borrar");
        str_codigo = UtilidadesLP.leerCadena();

        boolean c = objGestorLN.bln_borrarLibro(str_codigo);

        if (c) 
        {
            System.out.println("Libro borrado correctamente");
        } else 
        {
            System.out.println("Error, no se ha podido borrar el libro");
        }

    }

    /**
     * Metodo para visualizar libros de la biblioteca
     */
    private void vo_visualizarLibros() 
    {

        ArrayList<itfProperty> ListaLibros;
        boolean bln_respuesta;

        ListaLibros = objGestorLN.obj_recuperarArticulos();

        for (itfProperty itfArticulo : ListaLibros) 
        {
            bln_respuesta = objGestorLN.bln_comprobarLibro(itfArticulo);

            if (bln_respuesta) 
            {
                Object obj_titulo = itfArticulo.getObjectProperty(clsConstantes.TITULO);
                System.out.println("_____________________________________");
                System.out.println("Titulo: " + obj_titulo);

                Object obj_genero = itfArticulo.getObjectProperty(clsConstantes.GENERO);
                System.out.println("Genero: " + obj_genero);

                Object obj_precio = itfArticulo.getObjectProperty(clsConstantes.PRECIO);
                System.out.println("Precio: " + obj_precio);

                Object obj_codigo = itfArticulo.getObjectProperty(clsConstantes.CODIGO);
                System.out.println("Codigo: " + obj_codigo);

                Object obj_editorial = itfArticulo.getObjectProperty(clsConstantes.EDITORIAL);
                System.out.println("Editorial: " + obj_editorial);
                System.out.println("_____________________________________");
            }
        }
    }

    /**
     * Metodo para modificar los libros
     */
    private void vo_modificarLibro() 
    {
        int int_opcion = -1;
        String str_codigo;

        String str_newCodigo;
        String str_newTitulo;
        enTipoGenero en_newGenero;
        int int_newPrecio;
        String str_newEditorial;

        boolean bln_resGenero;
        boolean bln_resTitulo;
        boolean bln_resCodigo;
        boolean bln_resPrecio;
        boolean bln_resEditorial;

        vo_visualizarLibros();

        System.out.println("Escoge el codigo del libro que desas modificar");
        str_codigo = UtilidadesLP.leerCadena();

        try 
        {
            objGestorLN.bln_buscarArticulo(str_codigo);
        } 
        catch (clsCodigoIncorrecto e) 
        {
            System.out.println(e.mensajeError());
            int_opcion = 6;
        }

        while (int_opcion != 6) 
        {
            System.out.println("Datos del libro que desea modificar:");
            System.out.println("    1. Codigo");
            System.out.println("    2. Titulo");
            System.out.println("    3. Genero");
            System.out.println("    4. Precio");
            System.out.println("    5. Editorial");
            System.out.println("    6. Salir");

            int_opcion = UtilidadesLP.leerEntero();

            switch (int_opcion) 
            {
                case 1:
                    System.out.println("Introduce el nuevo codigo:");
                    str_newCodigo = UtilidadesLP.leerCadena();
                    bln_resCodigo = objGestorLN.bln_ModificarCodigoLibro(
                        str_codigo, str_newCodigo);
                    if (bln_resCodigo) 
                    {
                        System.out.println("Se ha cambiado el Codigo");
                    } 
                    else 
                    {
                        System.out.println("No se ha cambiado el codigo");
                    }
                    int_opcion = 6;
                    break;

                case 2:
                    System.out.println("Introduce el nuevo titulo:");
                    str_newTitulo = UtilidadesLP.leerCadena();
                    bln_resTitulo = objGestorLN.bln_ModificarTituloLibro(
                        str_codigo, str_newTitulo);
                    if (bln_resTitulo) 
                    {
                        System.out.println("Titulo cambiado");
                    } else 
                    {
                        System.out.println("Titulo no cambiado");
                    }
                    int_opcion = 6;
                    break;

                case 3:
                    en_newGenero = en_seleccionarTipo();
                    bln_resGenero = objGestorLN.bln_ModificarGeneroLibro(
                        str_codigo, en_newGenero);
                    if (bln_resGenero) 
                    {
                        System.out.println("Genero modificado !");
                    } else 
                    {
                        System.out.println("Genero no modificado");
                    }
                    int_opcion = 6;
                    break;

                case 4:
                    System.out.println("Introduce el nuevo precio:");
                    int_newPrecio = UtilidadesLP.leerEntero();
                    bln_resPrecio = objGestorLN.bln_ModificarPrecioLibro(
                        str_codigo, int_newPrecio);
                    if (bln_resPrecio) 
                    {
                        System.out.println("El Precio se ha cambiado");
                    } else 
                    {
                        System.out.println("No se ha cambiado el precio");
                    }
                    int_opcion = 6;
                    break;

                case 5:
                    System.out.println("Introduce la nueva editorial:");
                    str_newEditorial = UtilidadesLP.leerCadena();
                    bln_resEditorial = objGestorLN.bln_ModificarEditorialLibro(
                        str_codigo, str_newEditorial);
                    if (bln_resEditorial) 
                    {
                        System.out.println("Editorial Modificada");
                    } else 
                    {
                        System.out.println("No se ha cambiado la Editorial");
                    }
                    int_opcion = 6;
                    break;

                default:
                    System.out.println("Opcion no valida");
                    break;
            }
        }
    }

    /**
     * Metodo para seleccionar el tipo de genero
     * @return El tipo de genero
     */
    public enTipoGenero en_seleccionarTipo() 
    {
        int int_opcionEnum = -1;
        enTipoGenero en_genero = null;
        {
            System.out.println("Introduce el nuevo genero:");
            System.out.println("    1. Historico");
            System.out.println("    2. Terror");
            System.out.println("    3. Accion");
            System.out.println("    4. Romantico");

            int_opcionEnum = UtilidadesLP.leerEntero();

            switch (int_opcionEnum) 
            {
                case 1:
                    en_genero = enTipoGenero.en_historico;
                    break;
                case 2:
                    en_genero = enTipoGenero.en_terror;
                    break;
                case 3:
                    en_genero = enTipoGenero.en_accion;
                    break;
                case 4:
                    en_genero = enTipoGenero.en_romantico;
                    break;
                default:
                    System.out.println("Selecciona una opcion valida:");
                    break;
            }
        }
        return en_genero;
    }

    /**
     * Metodo para agregar musica a la biblioteca
     */
    private void vo_agregarMusica() 
    {
        String str_titulo;
        enTipoGenero en_Genero;
        int int_precio;
        String str_codigo;
        int int_anyo;
        String str_opcionGenero;

        en_Genero = null;

        System.out.print("Inserta el titulo: ");
        str_titulo = UtilidadesLP.leerCadena();

        while (en_Genero==null) 
        {
            System.out.println("Elige el genero:");
            System.out.println("a: Historico");
            System.out.println("b: Terror");
            System.out.println("c: Accion");
            System.out.println("d: Romantico");
    
            str_opcionGenero = UtilidadesLP.leerCadena();
    
            switch (str_opcionGenero) 
            {
                case "a":
                    en_Genero = enTipoGenero.en_historico;
                    break;
    
                case "b":
                    en_Genero = enTipoGenero.en_terror;
                    break;
    
                case "c":
                    en_Genero = enTipoGenero.en_accion;
                    break;
    
                case "d":
                    en_Genero = enTipoGenero.en_romantico;
                    break;
    
                default:
                    System.out.println("Seleccione una opcion valida...");
            }
        }

        System.out.print("Inserta el precio: ");
        int_precio = UtilidadesLP.leerEntero();

        System.out.print("Inserta el codigo: ");
        str_codigo = UtilidadesLP.leerCadena();

        System.out.print("Inserta el anyo: ");
        int_anyo = UtilidadesLP.leerEntero();

        try
        {
            objGestorLN.vo_InsertarMusica(
                str_titulo, en_Genero, int_precio, str_codigo, int_anyo);
            System.out.println("Musica insertada correctamente");
        }
        catch (clsArticuloRepetido e)
        {
            System.out.println(e.mensajeError());
        }
    }

    /**
     * Meotdo para borrar musica de la biblioteca
     */
    private void vo_borrarMusica() 
    {
        String str_codigo;
        boolean c;

        vo_visualizarMusica();

        System.out.println("Escoge el codigo de la musica que desas borrar");
        str_codigo = UtilidadesLP.leerCadena();

        c = objGestorLN.bln_borrarMusica(str_codigo);

        if (c) 
        {
            System.out.println("Musica borrado correctamente");
        } else 
        {
            System.out.println("Error, no se ha podido borrar la musica");
        }
    }

    /**
     * Metodo para visualizar la musica que hay en la biblioteca
     */
    private void vo_visualizarMusica() 
    {
        boolean bln_respuesta;

        ArrayList<itfProperty> ListaMusica = objGestorLN.obj_recuperarArticulos();

        for (itfProperty itfArticulo : ListaMusica) 
        {
            bln_respuesta = objGestorLN.bln_comprobarMusica(itfArticulo);

            if (bln_respuesta) 
            {
                Object obj_titulo = 
                itfArticulo.getObjectProperty(clsConstantes.TITULO);
                System.out.println("_____________________________________");
                System.out.println("Titulo: " + obj_titulo);

                Object obj_genero = 
                itfArticulo.getObjectProperty(clsConstantes.GENERO);
                System.out.println("Genero: " + obj_genero);

                Object obj_precio = 
                itfArticulo.getObjectProperty(clsConstantes.PRECIO);
                System.out.println("Precio: " + obj_precio);

                Object obj_codigo = 
                itfArticulo.getObjectProperty(clsConstantes.CODIGO);
                System.out.println("Codigo: " + obj_codigo);

                Object obj_anyo =
                 itfArticulo.getObjectProperty(clsConstantes.ANYO);
                System.out.println("Anyo: " + obj_anyo);
                System.out.println("_____________________________________");
            }
        }
    }


    /**
     * Metodo para agregar peliculas a la biblioteca
     */
    private void vo_agregarPelicula() 
    {
        String str_titulo;
        enTipoGenero en_Genero;
        int int_precio;
        String str_codigo;
        String str_director;
        String str_opcionGenero;

        en_Genero = null;

        System.out.print("Inserta el titulo: ");
        str_titulo = UtilidadesLP.leerCadena();

        while (en_Genero == null) 
        {
            System.out.println("Elige el genero:");
        System.out.println("a: Historico");
        System.out.println("b: Terror");
        System.out.println("c: Accion");
        System.out.println("d: Romantico");

        str_opcionGenero = UtilidadesLP.leerCadena();

        switch (str_opcionGenero) 
        {
            case "a":
                en_Genero = enTipoGenero.en_historico;
                break;

            case "b":
                en_Genero = enTipoGenero.en_terror;
                break;

            case "c":
                en_Genero = enTipoGenero.en_accion;
                break;

            case "d":
                en_Genero = enTipoGenero.en_romantico;
                break;

            default:
                System.out.println("Seleccione una opcion valida...");
        }
    }

        System.out.print("Inserta el precio: ");
        int_precio = UtilidadesLP.leerEntero();

        System.out.print("Inserta el codigo: ");
        str_codigo = UtilidadesLP.leerCadena();

        System.out.print("Inserta el director: ");
        str_director = UtilidadesLP.leerCadena();

        try 
        {
            objGestorLN.vo_InsertarPelicula(str_titulo, 
            en_Genero, int_precio, str_codigo, str_director);
            System.out.println("Pelicula insertada correctamente");
        } 
        catch (clsArticuloRepetido e) 
        {
            System.out.println(e.mensajeError());
        }

    }

    /**
     * Metodo para borrar peliculas de la biblioteca
     */
    private void vo_borrarPelicula() 
    {
        String str_codigo;
        boolean c;
        vo_visualizarPelicula();

        System.out.println("Escoge el codigo de la pelicula a borrar");
        str_codigo = UtilidadesLP.leerCadena();

        c = objGestorLN.bln_borrarPelicula(str_codigo);

        if (c) 
        {
            System.out.println("Pelicula borrado correctamente");
        } else 
        {
            System.out.println("Error, no se ha podido borrar la Pelicula");
        }
    }

    /**
     * Metodo para visualizar las peliculas que hay en la biblioteca
     */
    private void vo_visualizarPelicula() 
    {
        boolean bln_respuesta;

        ArrayList<itfProperty> ListaPeliculas = 
        objGestorLN.obj_recuperarArticulos();

        for (itfProperty itfArticulo : ListaPeliculas) 
        {
            bln_respuesta = objGestorLN.bln_comprobarPelicula(itfArticulo);

            if (bln_respuesta) {
                Object obj_titulo = 
                itfArticulo.getObjectProperty(clsConstantes.TITULO);
                System.out.println("_____________________________________");
                System.out.println("Titulo: " + obj_titulo);

                Object obj_genero = 
                itfArticulo.getObjectProperty(clsConstantes.GENERO);
                System.out.println("Genero: " + obj_genero);

                Object obj_precio = 
                itfArticulo.getObjectProperty(clsConstantes.PRECIO);
                System.out.println("Precio: " + obj_precio);

                Object obj_codigo = 
                itfArticulo.getObjectProperty(clsConstantes.CODIGO);
                System.out.println("Codigo: " + obj_codigo);

                Object obj_director = 
                itfArticulo.getObjectProperty(clsConstantes.DIRECTOR);
                System.out.println("Editorial: " + obj_director);
                System.out.println("_____________________________________");
            }
        }
    }

    /**
     * Metodo para modificar peliculas
     */
    private void vo_modificarPelicula() 
    {
        int int_opcion = -1;
        String str_codigo;

        String str_newCodigo;
        String str_newTitulo;
        enTipoGenero en_newGenero;
        int int_newPrecio;
        String str_newDirector;

        boolean bln_resGenero;
        boolean bln_resTitulo;
        boolean bln_resCodigo;
        boolean bln_resPrecio;
        boolean bln_resDirector;

        vo_visualizarPelicula();

        System.out.println("Codigo de la Pelicula que deseas modificar: ");
        str_codigo = UtilidadesLP.leerCadena();

        try 
        {
            objGestorLN.bln_buscarArticulo(str_codigo);
        } 
        catch (clsCodigoIncorrecto e) 
        {
            System.out.println(e.mensajeError());
            int_opcion = 6;
        }

        while (int_opcion != 6) 
        {
            System.out.println("Datos de la pelicula que desea modificar:");
            System.out.println("    1. Codigo");
            System.out.println("    2. Titulo");
            System.out.println("    3. Genero");
            System.out.println("    4. Precio");
            System.out.println("    5. Director");
            System.out.println("    6. Salir");

            int_opcion = UtilidadesLP.leerEntero();

            switch (int_opcion) 
            {
                case 1:
                    System.out.println("Escribe el nuevo Codigo: ");
                    str_newCodigo = UtilidadesLP.leerCadena(); 

                    bln_resCodigo = 
                    objGestorLN.bln_ModificarCodigoPelicula(str_codigo, 
                                                          str_newCodigo);

                    if (bln_resCodigo) 
                    {
                        System.out.println("Se ha modificado el Codigo");    
                    }else
                    {
                        System.out.println("No se ha modificado el Codigo");
                    }
                    int_opcion = 6;
                    break;

                case 2: 
                    System.out.println("Escribe el nuevo Titulo: ");
                    str_newTitulo = UtilidadesLP.leerCadena();
                    bln_resTitulo = 
                    objGestorLN.bln_ModificarTituloPelicula(str_codigo, 
                                                          str_newTitulo);
                    
                    if (bln_resTitulo) 
                    {
                        System.out.println("Se ha modificado el Titulo");    
                    }else
                    {
                        System.out.println("No se ha cambiado el Titulo");
                    }
                    int_opcion = 6;
                    break;

                case 3: 
                    en_newGenero = en_seleccionarTipo();
                    bln_resGenero = 
                    objGestorLN.bln_ModificarGeneroPelicula(str_codigo, 
                                                          en_newGenero);
                    if (bln_resGenero) 
                    {
                        System.out.println("Se ha modificado el genero");    
                    }else
                    {
                        System.out.println("No se ha modificado el genero");
                    }
                    int_opcion = 6;
                    break;

                case 4: 
                    System.out.println("Escribe el nuevo precio");
                    int_newPrecio = UtilidadesLP.leerEntero();
                    bln_resPrecio = 
                    objGestorLN.bln_ModificarPrecioPelicula(str_codigo, 
                                                          int_newPrecio);
                    if (bln_resPrecio) 
                    {
                        System.out.println("El precio ha sido modificado");    
                    }else
                    {
                        System.out.println("No se ha modificado el precio");
                    }
                    int_opcion = 6;
                    break;

                case 5: 
                    System.out.println("Escribe el nuevo Director");
                    str_newDirector = UtilidadesLP.leerCadena();
                    bln_resDirector = 
                    objGestorLN.bln_ModificarDirectorPelicula(str_codigo, 
                                                          str_newDirector);
                    if (bln_resDirector) 
                    {
                        System.out.println("Se ha modificado el Director");    
                    }else
                    {
                        System.out.println("No se ha modificado el Director");
                    }
                    int_opcion = 6;
                    break;
                    
                case 6:
                    System.out.println("Has salido");
                    int_opcion = 6;
                    break;
                default:
                    System.out.println("Introduce una opción valida");
                    break;
            }
        }
    }
}
