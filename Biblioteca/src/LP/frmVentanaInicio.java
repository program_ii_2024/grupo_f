package LP;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import LN.clsGestorLN;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class frmVentanaInicio extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	private clsGestorLN gln;

	/**
	 * Create the frame.
	 */
	public frmVentanaInicio() {
		gln = new clsGestorLN();

		setTitle("            Ventana Inicio");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);

		textField = new JTextField();
		textField.setBounds(155, 86, 128, 19);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(155, 143, 128, 19);
		textField_1.setColumns(10);
		contentPane.add(textField_1);

		JLabel lblNewLabel = new JLabel("Usuario");
		lblNewLabel.setBounds(155, 61, 128, 14);
		contentPane.add(lblNewLabel);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(155, 118, 128, 14);
		contentPane.add(lblPassword);

		JButton btnNewButton = new JButton("Iniciar Sesion");
		btnNewButton.setBounds(225, 193, 128, 23);
		contentPane.add(btnNewButton);

		JButton btnVisitante = new JButton("Visitante");
		btnVisitante.setBounds(87, 193, 128, 23);
		contentPane.add(btnVisitante);
		setVisible(true);

		btnVisitante.setActionCommand("Visitante");
		btnNewButton.setActionCommand("Iniciar Sesion");
		btnVisitante.addActionListener(this);
		btnNewButton.addActionListener(this);

	}

	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
			case "Visitante":

				System.out.println("DEBUG: Visitante");
				setVisible(false);
				new frmVentanaVisitante(gln);
				break;

			case "Iniciar Sesion":

				if (gln.bln_comprobarUsuario(textField.getText(), Integer.parseInt(textField_1.getText()))) {
					if (gln.bln_esUsuarioRegistrado(textField.getText())) {
						setVisible(false);
						new frmVentanaRegistrado(gln);
						break;
					} else if (gln.bln_esUsuarioBibliotecario(textField.getText())) {
						setVisible(false);
						new frmVentanaAdmin(gln);
						break;
					} else {
						System.out.println("DEBUG: Usuario no encontrado");
					}
				} else {
					System.out.println("DEBUG: No existe este usuario");
				}
				break;

			default:
				break;

		}
	}
}
